using System.Collections.Generic;
using System.Linq;

namespace Verse
{
	public class BodyPartRecord
	{
		public BodyDef body;

		[TranslationHandle]
		public BodyPartDef def;

		[MustTranslate]
		public string customLabel;

		[Unsaved]
		[TranslationHandle(Priority = 100)]
		public string untranslatedCustomLabel;

		public List<BodyPartRecord> parts = new List<BodyPartRecord>();

		public BodyPartHeight height;

		public BodyPartDepth depth;

		public float coverage = 1f;

		public List<BodyPartGroupDef> groups = new List<BodyPartGroupDef>();

		[Unsaved]
		public BodyPartRecord parent;

		[Unsaved]
		public float coverageAbsWithChildren;

		[Unsaved]
		public float coverageAbs;

		public bool IsCorePart => parent == null;

		public string Label => (!customLabel.NullOrEmpty()) ? customLabel : def.label;

		public string LabelCap => Label.CapitalizeFirst();

		public string LabelShort => def.LabelShort;

		public string LabelShortCap => def.LabelShortCap;

		public int Index => body.GetIndexOfPart(this);

		public override string ToString()
		{
			return "BodyPartRecord(" + ((def == null) ? "NULL_DEF" : def.defName) + " parts.Count=" + parts.Count + ")";
		}

		public void PostLoad()
		{
			untranslatedCustomLabel = customLabel;
		}

		public bool IsInGroup(BodyPartGroupDef group)
		{
			for (int i = 0; i < groups.Count; i++)
			{
				if (groups[i] == group)
				{
					return true;
				}
			}
			return false;
		}

		public IEnumerable<BodyPartRecord> GetChildParts(BodyPartTagDef tag)
		{
			if (def.tags.Contains(tag))
			{
				yield return this;
			}
			for (int i = 0; i < parts.Count; i++)
			{
				foreach (BodyPartRecord childPart in parts[i].GetChildParts(tag))
				{
					yield return childPart;
				}
			}
		}

		public IEnumerable<BodyPartRecord> GetDirectChildParts()
		{
			for (int i = 0; i < parts.Count; i++)
			{
				yield return parts[i];
			}
		}

		public bool HasChildParts(BodyPartTagDef tag)
		{
			return GetChildParts(tag).Any();
		}

		public IEnumerable<BodyPartRecord> GetConnectedParts(BodyPartTagDef tag)
		{
			BodyPartRecord ancestor = this;
			while (ancestor.parent != null && ancestor.parent.def.tags.Contains(tag))
			{
				ancestor = ancestor.parent;
			}
			foreach (BodyPartRecord childPart in ancestor.GetChildParts(tag))
			{
				yield return childPart;
			}
		}
	}
}
