using System;
using System.Threading;

namespace Verse
{
	public static class GenThreading
	{
		public static void ParallelFor(int fromInclusive, int toExclusive, Action<int> callback)
		{
			int num = toExclusive - fromInclusive;
			long tasksDone = 0L;
			AutoResetEvent autoResetEvent = new AutoResetEvent(initialState: false);
			for (int j = fromInclusive; j < toExclusive; j++)
			{
				int i = j;
				ThreadPool.QueueUserWorkItem(delegate
				{
					callback(i);
					Interlocked.Increment(ref tasksDone);
				});
			}
			while (Interlocked.Read(ref tasksDone) < num)
			{
				autoResetEvent.WaitOne(10);
			}
		}
	}
}
