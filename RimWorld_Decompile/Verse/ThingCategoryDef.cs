using System.Collections.Generic;
using UnityEngine;

namespace Verse
{
	public class ThingCategoryDef : Def
	{
		public ThingCategoryDef parent;

		[NoTranslate]
		public string iconPath;

		public bool resourceReadoutRoot;

		[Unsaved]
		public TreeNode_ThingCategory treeNode;

		[Unsaved]
		public List<ThingCategoryDef> childCategories = new List<ThingCategoryDef>();

		[Unsaved]
		public List<ThingDef> childThingDefs = new List<ThingDef>();

		[Unsaved]
		private HashSet<ThingDef> allChildThingDefsCached;

		[Unsaved]
		public List<SpecialThingFilterDef> childSpecialFilters = new List<SpecialThingFilterDef>();

		[Unsaved]
		public Texture2D icon = BaseContent.BadTex;

		public IEnumerable<ThingCategoryDef> Parents
		{
			get
			{
				if (parent != null)
				{
					yield return parent;
					foreach (ThingCategoryDef parent2 in parent.Parents)
					{
						yield return parent2;
					}
				}
			}
		}

		public IEnumerable<ThingCategoryDef> ThisAndChildCategoryDefs
		{
			get
			{
				yield return this;
				foreach (ThingCategoryDef child in childCategories)
				{
					foreach (ThingCategoryDef thisAndChildCategoryDef in child.ThisAndChildCategoryDefs)
					{
						yield return thisAndChildCategoryDef;
					}
				}
			}
		}

		public IEnumerable<ThingDef> DescendantThingDefs
		{
			get
			{
				foreach (ThingCategoryDef childCatDef in ThisAndChildCategoryDefs)
				{
					foreach (ThingDef childThingDef in childCatDef.childThingDefs)
					{
						yield return childThingDef;
					}
				}
			}
		}

		public IEnumerable<SpecialThingFilterDef> DescendantSpecialThingFilterDefs
		{
			get
			{
				foreach (ThingCategoryDef childCatDef in ThisAndChildCategoryDefs)
				{
					foreach (SpecialThingFilterDef childSpecialFilter in childCatDef.childSpecialFilters)
					{
						yield return childSpecialFilter;
					}
				}
			}
		}

		public IEnumerable<SpecialThingFilterDef> ParentsSpecialThingFilterDefs
		{
			get
			{
				foreach (ThingCategoryDef cat in Parents)
				{
					foreach (SpecialThingFilterDef childSpecialFilter in cat.childSpecialFilters)
					{
						yield return childSpecialFilter;
					}
				}
			}
		}

		public bool ContainedInThisOrDescendant(ThingDef thingDef)
		{
			if (allChildThingDefsCached == null)
			{
				allChildThingDefsCached = new HashSet<ThingDef>();
				foreach (ThingCategoryDef thisAndChildCategoryDef in ThisAndChildCategoryDefs)
				{
					foreach (ThingDef childThingDef in thisAndChildCategoryDef.childThingDefs)
					{
						allChildThingDefsCached.Add(childThingDef);
					}
				}
			}
			return allChildThingDefsCached.Contains(thingDef);
		}

		public override void PostLoad()
		{
			treeNode = new TreeNode_ThingCategory(this);
			if (!iconPath.NullOrEmpty())
			{
				LongEventHandler.ExecuteWhenFinished(delegate
				{
					icon = ContentFinder<Texture2D>.Get(iconPath);
				});
			}
		}

		public static ThingCategoryDef Named(string defName)
		{
			return DefDatabase<ThingCategoryDef>.GetNamed(defName);
		}

		public override int GetHashCode()
		{
			return defName.GetHashCode();
		}
	}
}
