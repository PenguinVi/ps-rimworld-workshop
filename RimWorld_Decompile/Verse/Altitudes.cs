using UnityEngine;

namespace Verse
{
	public static class Altitudes
	{
		private const int NumAltitudeLayers = 31;

		private static readonly float[] Alts;

		private const float LayerSpacing = 15f / 32f;

		public const float AltInc = 3f / 64f;

		public static readonly Vector3 AltIncVect;

		static Altitudes()
		{
			Alts = new float[31];
			AltIncVect = new Vector3(0f, 3f / 64f, 0f);
			for (int i = 0; i < 31; i++)
			{
				Alts[i] = (float)i * (15f / 32f);
			}
		}

		public static float AltitudeFor(this AltitudeLayer alt)
		{
			return Alts[(uint)alt];
		}
	}
}
