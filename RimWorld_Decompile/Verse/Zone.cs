using RimWorld;
using System;
using System.Collections.Generic;
using UnityEngine;
using Verse.Sound;

namespace Verse
{
	public abstract class Zone : IExposable, ISelectable, ILoadReferenceable
	{
		public ZoneManager zoneManager;

		public int ID = -1;

		public string label;

		public List<IntVec3> cells = new List<IntVec3>();

		private bool cellsShuffled;

		public Color color = Color.white;

		private Material materialInt;

		public bool hidden;

		private int lastStaticFireCheckTick = -9999;

		private bool lastStaticFireCheckResult;

		private const int StaticFireCheckInterval = 1000;

		private static BoolGrid extantGrid;

		private static BoolGrid foundGrid;

		public Map Map => zoneManager.map;

		public IntVec3 Position => (cells.Count == 0) ? IntVec3.Invalid : cells[0];

		public Material Material
		{
			get
			{
				if (materialInt == null)
				{
					materialInt = SolidColorMaterials.SimpleSolidColorMaterial(color);
					materialInt.renderQueue = 3600;
				}
				return materialInt;
			}
		}

		public List<IntVec3> Cells
		{
			get
			{
				if (!cellsShuffled)
				{
					cells.Shuffle();
					cellsShuffled = true;
				}
				return cells;
			}
		}

		public IEnumerable<Thing> AllContainedThings
		{
			get
			{
				ThingGrid grids = Map.thingGrid;
				for (int j = 0; j < cells.Count; j++)
				{
					List<Thing> thingList = grids.ThingsListAt(cells[j]);
					for (int i = 0; i < thingList.Count; i++)
					{
						yield return thingList[i];
					}
				}
			}
		}

		public bool ContainsStaticFire
		{
			get
			{
				if (Find.TickManager.TicksGame > lastStaticFireCheckTick + 1000)
				{
					lastStaticFireCheckResult = false;
					for (int i = 0; i < cells.Count; i++)
					{
						if (cells[i].ContainsStaticFire(Map))
						{
							lastStaticFireCheckResult = true;
							break;
						}
					}
				}
				return lastStaticFireCheckResult;
			}
		}

		public virtual bool IsMultiselectable => false;

		protected abstract Color NextZoneColor
		{
			get;
		}

		public Zone()
		{
		}

		public Zone(string baseName, ZoneManager zoneManager)
		{
			label = zoneManager.NewZoneName(baseName);
			this.zoneManager = zoneManager;
			ID = Find.UniqueIDsManager.GetNextZoneID();
			color = NextZoneColor;
		}

		public IEnumerator<IntVec3> GetEnumerator()
		{
			for (int i = 0; i < cells.Count; i++)
			{
				yield return cells[i];
			}
		}

		public virtual void ExposeData()
		{
			Scribe_Values.Look(ref ID, "ID", -1);
			Scribe_Values.Look(ref label, "label");
			Scribe_Values.Look(ref color, "color");
			Scribe_Values.Look(ref hidden, "hidden", defaultValue: false);
			Scribe_Collections.Look(ref cells, "cells", LookMode.Undefined);
			if (Scribe.mode == LoadSaveMode.PostLoadInit)
			{
				BackCompatibility.ZonePostLoadInit(this);
				CheckAddHaulDestination();
			}
		}

		public virtual void AddCell(IntVec3 c)
		{
			if (cells.Contains(c))
			{
				Log.Error("Adding cell to zone which already has it. c=" + c + ", zone=" + this);
				return;
			}
			List<Thing> list = Map.thingGrid.ThingsListAt(c);
			for (int i = 0; i < list.Count; i++)
			{
				Thing thing = list[i];
				if (!thing.def.CanOverlapZones)
				{
					Log.Error("Added zone over zone-incompatible thing " + thing);
					return;
				}
			}
			cells.Add(c);
			zoneManager.AddZoneGridCell(this, c);
			Map.mapDrawer.MapMeshDirty(c, MapMeshFlag.Zone);
			AutoHomeAreaMaker.Notify_ZoneCellAdded(c, this);
			cellsShuffled = false;
		}

		public virtual void RemoveCell(IntVec3 c)
		{
			if (!cells.Contains(c))
			{
				Log.Error("Cannot remove cell from zone which doesn't have it. c=" + c + ", zone=" + this);
				return;
			}
			cells.Remove(c);
			zoneManager.ClearZoneGridCell(c);
			Map.mapDrawer.MapMeshDirty(c, MapMeshFlag.Zone);
			cellsShuffled = false;
			if (cells.Count == 0)
			{
				Deregister();
			}
		}

		public virtual void Delete()
		{
			SoundDefOf.Designate_ZoneDelete.PlayOneShotOnCamera(Map);
			if (cells.Count == 0)
			{
				Deregister();
			}
			else
			{
				while (cells.Count > 0)
				{
					RemoveCell(cells[cells.Count - 1]);
				}
			}
			Find.Selector.Deselect(this);
		}

		public void Deregister()
		{
			zoneManager.DeregisterZone(this);
		}

		public virtual void PostRegister()
		{
			CheckAddHaulDestination();
		}

		public virtual void PostDeregister()
		{
			IHaulDestination haulDestination = this as IHaulDestination;
			if (haulDestination != null)
			{
				Map.haulDestinationManager.RemoveHaulDestination(haulDestination);
			}
		}

		public bool ContainsCell(IntVec3 c)
		{
			for (int i = 0; i < cells.Count; i++)
			{
				if (cells[i] == c)
				{
					return true;
				}
			}
			return false;
		}

		public virtual string GetInspectString()
		{
			return string.Empty;
		}

		public virtual IEnumerable<InspectTabBase> GetInspectTabs()
		{
			yield break;
		}

		public virtual IEnumerable<Gizmo> GetGizmos()
		{
			yield return new Command_Action
			{
				icon = ContentFinder<Texture2D>.Get("UI/Commands/RenameZone"),
				defaultLabel = "CommandRenameZoneLabel".Translate(),
				defaultDesc = "CommandRenameZoneDesc".Translate(),
				action = delegate
				{
					Find.WindowStack.Add(new Dialog_RenameZone(this));
				},
				hotKey = KeyBindingDefOf.Misc1
			};
			yield return new Command_Toggle
			{
				icon = ContentFinder<Texture2D>.Get("UI/Commands/HideZone"),
				defaultLabel = ((!hidden) ? "CommandHideZoneLabel".Translate() : "CommandUnhideZoneLabel".Translate()),
				defaultDesc = "CommandHideZoneDesc".Translate(),
				isActive = (() => hidden),
				toggleAction = delegate
				{
					hidden = !hidden;
					foreach (IntVec3 cell in Cells)
					{
						Map.mapDrawer.MapMeshDirty(cell, MapMeshFlag.Zone);
					}
				},
				hotKey = KeyBindingDefOf.Misc2
			};
			foreach (Gizmo zoneAddGizmo in GetZoneAddGizmos())
			{
				yield return zoneAddGizmo;
			}
			Designator delete = DesignatorUtility.FindAllowedDesignator<Designator_ZoneDelete_Shrink>();
			if (delete != null)
			{
				yield return delete;
			}
			yield return new Command_Action
			{
				icon = TexButton.DeleteX,
				defaultLabel = "CommandDeleteZoneLabel".Translate(),
				defaultDesc = "CommandDeleteZoneDesc".Translate(),
				action = Delete,
				hotKey = KeyBindingDefOf.Misc3
			};
		}

		public virtual IEnumerable<Gizmo> GetZoneAddGizmos()
		{
			yield break;
		}

		public void CheckContiguous()
		{
			if (cells.Count != 0)
			{
				if (extantGrid == null)
				{
					extantGrid = new BoolGrid(Map);
				}
				else
				{
					extantGrid.ClearAndResizeTo(Map);
				}
				if (foundGrid == null)
				{
					foundGrid = new BoolGrid(Map);
				}
				else
				{
					foundGrid.ClearAndResizeTo(Map);
				}
				for (int i = 0; i < cells.Count; i++)
				{
					extantGrid.Set(cells[i], value: true);
				}
				Predicate<IntVec3> passCheck = delegate(IntVec3 c)
				{
					if (!extantGrid[c])
					{
						return false;
					}
					if (foundGrid[c])
					{
						return false;
					}
					return true;
				};
				int numFound = 0;
				Action<IntVec3> processor = delegate(IntVec3 c)
				{
					foundGrid.Set(c, value: true);
					numFound++;
				};
				Map.floodFiller.FloodFill(cells[0], passCheck, processor);
				if (numFound < cells.Count)
				{
					foreach (IntVec3 allCell in Map.AllCells)
					{
						if (extantGrid[allCell] && !foundGrid[allCell])
						{
							RemoveCell(allCell);
						}
					}
				}
			}
		}

		private void CheckAddHaulDestination()
		{
			IHaulDestination haulDestination = this as IHaulDestination;
			if (haulDestination != null)
			{
				Map.haulDestinationManager.AddHaulDestination(haulDestination);
			}
		}

		public override string ToString()
		{
			return label;
		}

		public string GetUniqueLoadID()
		{
			return "Zone_" + ID;
		}
	}
}
