using RimWorld;
using Steamworks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using Verse.Steam;

namespace Verse
{
	public class ModMetaData : WorkshopUploadable
	{
		private class ModMetaDataInternal
		{
			public string name = string.Empty;

			public string author = "Anonymous";

			public string url = string.Empty;

			[Obsolete("Deprecated, will be removed in the future. Use SupportedVersions instead")]
			public string targetVersion = "Unknown";

			public string description = "No description provided.";

			public List<string> supportedVersions;

			public List<System.Version> SupportedVersions
			{
				get;
				private set;
			}

			private bool TryParseVersion(string str, bool logWarnings = true)
			{
				if (!VersionControl.TryParseVersionString(str, out System.Version version))
				{
					Log.Error("Unable to parse version string on mod " + name + " from " + author + " \"" + str + "\"");
					return false;
				}
				SupportedVersions.Add(version);
				if (logWarnings && !VersionControl.IsWellFormattedVersionString(str))
				{
					Log.Warning("Malformed (correct format is Major.Minor) version string on mod " + name + " from " + author + " \"" + str + "\" - parsed as \"" + version.Major.ToString() + "." + version.Minor.ToString() + "\"");
					return false;
				}
				return true;
			}

			public bool TryParseSupportedVersions(bool logWarnings = true)
			{
				bool flag = false;
				SupportedVersions = new List<System.Version>();
				if (name == ModContentPack.CoreModIdentifier)
				{
					SupportedVersions.Add(VersionControl.CurrentVersion);
				}
				else if (supportedVersions == null)
				{
					if (logWarnings)
					{
						Log.Warning("<targetVersion> in mod About.xml is deprecated! Use <supportedVersions> tag instead. (example: <supportedVersions><li>1.0</li></supportedVersions>)");
					}
					TryParseVersion(targetVersion, logWarnings);
					flag = true;
				}
				else if (supportedVersions.Count == 0)
				{
					Log.Error("<supportedVersions> in mod About.xml must specify at least one version.");
					flag = true;
				}
				else
				{
					for (int i = 0; i < supportedVersions.Count; i++)
					{
						flag |= !TryParseVersion(supportedVersions[i]);
					}
					if (logWarnings && targetVersion != "Unknown")
					{
						Log.Warning("<targetVersion> in mod About.xml is deprecated! Use <supportedVersions> tag instead. (example: <supportedVersions><li>1.0</li></supportedVersions>)");
						flag = true;
					}
				}
				SupportedVersions = (from v in SupportedVersions
				orderby (!VersionControl.IsCompatible(v)) ? 100 : (-100), v.Major descending, v.Minor descending
				select v).Distinct().ToList();
				return !flag;
			}
		}

		private DirectoryInfo rootDirInt;

		private ContentSource source;

		[Obsolete("This field will be made private in RimWorld 1.1 - please use PreviewImage property instead.")]
		public Texture2D previewImage;

		private bool previewImageWasLoaded;

		public bool enabled = true;

		private ModMetaDataInternal meta = new ModMetaDataInternal();

		private WorkshopItemHook workshopHookInt;

		private PublishedFileId_t publishedFileIdInt = PublishedFileId_t.Invalid;

		private const string AboutFolderName = "About";

		[CompilerGenerated]
		private static Predicate<System.Version> _003C_003Ef__mg_0024cache0;

		IEnumerable<System.Version> WorkshopUploadable.SupportedVersions
		{
			get
			{
				return SupportedVersionsReadOnly;
			}
		}

		public Texture2D PreviewImage
		{
			get
			{
				if (previewImageWasLoaded)
				{
					return previewImage;
				}
				if (File.Exists(PreviewImagePath))
				{
					previewImage = new Texture2D(0, 0);
					previewImage.LoadImage(File.ReadAllBytes(PreviewImagePath));
				}
				previewImageWasLoaded = true;
				return previewImage;
			}
		}

		public string Identifier => RootDir.Name;

		public DirectoryInfo RootDir => rootDirInt;

		public bool IsCoreMod => Identifier == ModContentPack.CoreModIdentifier;

		public bool Active
		{
			get
			{
				return ModsConfig.IsActive(this);
			}
			set
			{
				ModsConfig.SetActive(this, value);
			}
		}

		public bool VersionCompatible
		{
			get
			{
				if (IsCoreMod)
				{
					return true;
				}
				return meta.SupportedVersions.Any(VersionControl.IsCompatible);
			}
		}

		public bool MadeForNewerVersion
		{
			get
			{
				if (VersionCompatible)
				{
					return false;
				}
				return meta.SupportedVersions.Any((System.Version v) => v.Major > VersionControl.CurrentMajor || (v.Major == VersionControl.CurrentMajor && v.Minor > VersionControl.CurrentMinor));
			}
		}

		public string Name
		{
			get
			{
				return meta.name;
			}
			set
			{
				meta.name = value;
			}
		}

		public string Author => meta.author;

		public string Url => meta.url;

		public string Description => meta.description;

		[Obsolete("Deprecated, will be removed in the future. Use SupportedVersions instead")]
		public string TargetVersion
		{
			get
			{
				if (SupportedVersionsReadOnly.Count == 0)
				{
					return "Unknown";
				}
				System.Version version = meta.SupportedVersions[0];
				return version.Major + "." + version.Minor;
			}
		}

		public List<System.Version> SupportedVersionsReadOnly => meta.SupportedVersions;

		public string PreviewImagePath => rootDirInt.FullName + Path.DirectorySeparatorChar + "About" + Path.DirectorySeparatorChar + "Preview.png";

		public ContentSource Source => source;

		public bool HadIncorrectlyFormattedVersionInMetadata
		{
			get;
			private set;
		}

		public bool OnSteamWorkshop => source == ContentSource.SteamWorkshop;

		private string PublishedFileIdPath => rootDirInt.FullName + Path.DirectorySeparatorChar + "About" + Path.DirectorySeparatorChar + "PublishedFileId.txt";

		public ModMetaData()
		{
			previewImage = BaseContent.BadTex;
		}

		public ModMetaData(string localAbsPath)
			: this()
		{
			rootDirInt = new DirectoryInfo(localAbsPath);
			source = ContentSource.LocalFolder;
			Init();
		}

		public ModMetaData(WorkshopItem workshopItem)
			: this()
		{
			rootDirInt = workshopItem.Directory;
			source = ContentSource.SteamWorkshop;
			Init();
		}

		private void Init()
		{
			meta = DirectXmlLoader.ItemFromXmlFile<ModMetaDataInternal>(RootDir.FullName + Path.DirectorySeparatorChar + "About" + Path.DirectorySeparatorChar + "About.xml");
			HadIncorrectlyFormattedVersionInMetadata = !meta.TryParseSupportedVersions(!OnSteamWorkshop);
			if (meta.name.NullOrEmpty())
			{
				if (OnSteamWorkshop)
				{
					meta.name = "Workshop mod " + Identifier;
				}
				else
				{
					meta.name = Identifier;
				}
			}
			string publishedFileIdPath = PublishedFileIdPath;
			if (File.Exists(PublishedFileIdPath))
			{
				string s = File.ReadAllText(publishedFileIdPath);
				publishedFileIdInt = new PublishedFileId_t(ulong.Parse(s));
			}
		}

		internal void DeleteContent()
		{
			rootDirInt.Delete(recursive: true);
			ModLister.RebuildModList();
		}

		public void PrepareForWorkshopUpload()
		{
		}

		public bool CanToUploadToWorkshop()
		{
			if (IsCoreMod)
			{
				return false;
			}
			if (Source != ContentSource.LocalFolder)
			{
				return false;
			}
			if (GetWorkshopItemHook().MayHaveAuthorNotCurrentUser)
			{
				return false;
			}
			return true;
		}

		public PublishedFileId_t GetPublishedFileId()
		{
			return publishedFileIdInt;
		}

		public void SetPublishedFileId(PublishedFileId_t newPfid)
		{
			if (!(publishedFileIdInt == newPfid))
			{
				publishedFileIdInt = newPfid;
				File.WriteAllText(PublishedFileIdPath, newPfid.ToString());
			}
		}

		public string GetWorkshopName()
		{
			return Name;
		}

		public string GetWorkshopDescription()
		{
			return Description;
		}

		public string GetWorkshopPreviewImagePath()
		{
			return PreviewImagePath;
		}

		public IList<string> GetWorkshopTags()
		{
			List<string> list = new List<string>();
			list.Add("Mod");
			return list;
		}

		public DirectoryInfo GetWorkshopUploadDirectory()
		{
			return RootDir;
		}

		public WorkshopItemHook GetWorkshopItemHook()
		{
			if (workshopHookInt == null)
			{
				workshopHookInt = new WorkshopItemHook(this);
			}
			return workshopHookInt;
		}

		public override int GetHashCode()
		{
			return Identifier.GetHashCode();
		}

		public override string ToString()
		{
			return "[" + Identifier + "|" + Name + "]";
		}

		public string ToStringLong()
		{
			return Identifier + "(" + RootDir.ToString() + ")";
		}
	}
}
