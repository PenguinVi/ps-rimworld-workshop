using RimWorld;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using UnityEngine;

namespace Verse
{
	public static class DirectXmlLoader
	{
		public static IEnumerable<LoadableXmlAsset> XmlAssetsInModFolder(ModContentPack mod, string folderPath)
		{
			DirectoryInfo di = new DirectoryInfo(Path.Combine(mod.RootDir, folderPath));
			DirectoryInfo diVersionSpecific = new DirectoryInfo(Path.Combine(Path.Combine(mod.RootDir, VersionControl.CurrentVersionStringWithoutBuild), folderPath));
			DirectoryInfo xmlDirectory = (!diVersionSpecific.Exists) ? di : diVersionSpecific;
			if (xmlDirectory.Exists)
			{
				FileInfo[] files = xmlDirectory.GetFiles("*.xml", SearchOption.AllDirectories);
				FileInfo[] array = files;
				foreach (FileInfo file in array)
				{
					yield return new LoadableXmlAsset(file.Name, file.Directory.FullName, File.ReadAllText(file.FullName))
					{
						mod = mod
					};
				}
			}
		}

		public static IEnumerable<T> LoadXmlDataInResourcesFolder<T>(string folderPath) where T : new()
		{
			XmlInheritance.Clear();
			List<LoadableXmlAsset> assets = new List<LoadableXmlAsset>();
			object[] textObjects = Resources.LoadAll<TextAsset>(folderPath);
			object[] array = textObjects;
			for (int j = 0; j < array.Length; j++)
			{
				TextAsset textAsset = (TextAsset)array[j];
				LoadableXmlAsset loadableXmlAsset = new LoadableXmlAsset(textAsset.name, string.Empty, textAsset.text);
				XmlInheritance.TryRegisterAllFrom(loadableXmlAsset, null);
				assets.Add(loadableXmlAsset);
			}
			XmlInheritance.Resolve();
			for (int i = 0; i < assets.Count; i++)
			{
				foreach (T item in AllGameItemsFromAsset<T>(assets[i]))
				{
					yield return item;
				}
			}
			XmlInheritance.Clear();
		}

		public static T ItemFromXmlFile<T>(string filePath, bool resolveCrossRefs = true) where T : new()
		{
			if (resolveCrossRefs && DirectXmlCrossRefLoader.LoadingInProgress)
			{
				Log.Error("Cannot call ItemFromXmlFile with resolveCrossRefs=true while loading is already in progress.");
			}
			FileInfo fileInfo = new FileInfo(filePath);
			if (!fileInfo.Exists)
			{
				return new T();
			}
			try
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(File.ReadAllText(fileInfo.FullName));
				T result = DirectXmlToObject.ObjectFromXml<T>(xmlDocument.DocumentElement, doPostLoad: false);
				if (resolveCrossRefs)
				{
					DirectXmlCrossRefLoader.ResolveAllWantedCrossReferences(FailMode.LogErrors);
				}
				return result;
			}
			catch (Exception ex)
			{
				Log.Error("Exception loading file at " + filePath + ". Loading defaults instead. Exception was: " + ex.ToString());
				return new T();
			}
		}

		public static Def DefFromNode(XmlNode node, LoadableXmlAsset loadingAsset)
		{
			if (node.NodeType != XmlNodeType.Element)
			{
				return null;
			}
			XmlAttribute xmlAttribute = node.Attributes["Abstract"];
			if (xmlAttribute != null && xmlAttribute.Value.ToLower() == "true")
			{
				return null;
			}
			Type typeInAnyAssembly = GenTypes.GetTypeInAnyAssembly(node.Name);
			if (typeInAnyAssembly == null)
			{
				return null;
			}
			if (!typeof(Def).IsAssignableFrom(typeInAnyAssembly))
			{
				return null;
			}
			MethodInfo objectFromXmlMethod = DirectXmlToObject.GetObjectFromXmlMethod(typeInAnyAssembly);
			Def result = null;
			try
			{
				result = (Def)objectFromXmlMethod.Invoke(null, new object[2]
				{
					node,
					true
				});
				return result;
			}
			catch (Exception ex)
			{
				Log.Error("Exception loading def from file " + ((loadingAsset == null) ? "(unknown)" : loadingAsset.name) + ": " + ex);
				return result;
			}
		}

		public static IEnumerable<T> AllGameItemsFromAsset<T>(LoadableXmlAsset asset) where T : new()
		{
			if (asset.xmlDoc != null)
			{
				XmlNodeList assetNodes = asset.xmlDoc.DocumentElement.SelectNodes(typeof(T).Name);
				bool gotData = false;
				IEnumerator enumerator = assetNodes.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						XmlNode node = (XmlNode)enumerator.Current;
						XmlAttribute abstractAtt = node.Attributes["Abstract"];
						if (abstractAtt == null || !(abstractAtt.Value.ToLower() == "true"))
						{
							T item;
							try
							{
								item = DirectXmlToObject.ObjectFromXml<T>(node, doPostLoad: true);
								gotData = true;
							}
							catch (Exception ex)
							{
								Log.Error("Exception loading data from file " + asset.name + ": " + ex);
								continue;
							}
							yield return item;
						}
					}
				}
				finally
				{
					IDisposable disposable;
					IDisposable disposable2 = disposable = (enumerator as IDisposable);
					if (disposable != null)
					{
						disposable2.Dispose();
					}
				}
				if (!gotData)
				{
					Log.Error("Found no usable data when trying to get " + typeof(T) + "s from file " + asset.name);
				}
			}
		}
	}
}
