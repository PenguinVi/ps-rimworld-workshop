using System.Collections.Generic;

namespace Verse
{
	public class PawnInventoryOption
	{
		public ThingDef thingDef;

		public IntRange countRange = IntRange.one;

		public float choiceChance = 1f;

		public float skipChance;

		public List<PawnInventoryOption> subOptionsTakeAll;

		public List<PawnInventoryOption> subOptionsChooseOne;

		public IEnumerable<Thing> GenerateThings()
		{
			if (!(Rand.Value < skipChance))
			{
				if (thingDef != null && countRange.max > 0)
				{
					Thing thing = ThingMaker.MakeThing(thingDef);
					thing.stackCount = countRange.RandomInRange;
					yield return thing;
				}
				if (subOptionsTakeAll != null)
				{
					foreach (PawnInventoryOption opt in subOptionsTakeAll)
					{
						foreach (Thing item in opt.GenerateThings())
						{
							yield return item;
						}
					}
				}
				if (subOptionsChooseOne != null)
				{
					PawnInventoryOption chosen = subOptionsChooseOne.RandomElementByWeight((PawnInventoryOption o) => o.choiceChance);
					foreach (Thing item2 in chosen.GenerateThings())
					{
						yield return item2;
					}
				}
			}
		}
	}
}
