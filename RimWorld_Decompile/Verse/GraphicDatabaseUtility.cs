using System.Collections.Generic;
using UnityEngine;

namespace Verse
{
	public static class GraphicDatabaseUtility
	{
		public static IEnumerable<string> GraphicNamesInFolder(string folderPath)
		{
			HashSet<string> loadedAssetNames = new HashSet<string>();
			Texture2D[] array = Resources.LoadAll<Texture2D>("Textures/" + folderPath);
			foreach (Texture2D tex in array)
			{
				string origAssetName = tex.name;
				string[] pieces = origAssetName.Split('_');
				string assetName = string.Empty;
				if (pieces.Length <= 2)
				{
					assetName = pieces[0];
				}
				else if (pieces.Length == 3)
				{
					assetName = pieces[0] + "_" + pieces[1];
				}
				else if (pieces.Length == 4)
				{
					assetName = pieces[0] + "_" + pieces[1] + "_" + pieces[2];
				}
				else
				{
					Log.Error("Cannot load assets with >3 pieces.");
				}
				if (!loadedAssetNames.Contains(assetName))
				{
					loadedAssetNames.Add(assetName);
					yield return assetName;
				}
			}
		}
	}
}
