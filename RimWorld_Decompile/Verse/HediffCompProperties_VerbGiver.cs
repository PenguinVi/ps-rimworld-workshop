using System.Collections.Generic;
using System.Linq;

namespace Verse
{
	public class HediffCompProperties_VerbGiver : HediffCompProperties
	{
		public List<VerbProperties> verbs;

		public List<Tool> tools;

		public HediffCompProperties_VerbGiver()
		{
			compClass = typeof(HediffComp_VerbGiver);
		}

		public override void PostLoad()
		{
			base.PostLoad();
			if (tools != null)
			{
				for (int i = 0; i < tools.Count; i++)
				{
					tools[i].id = i.ToString();
				}
			}
		}

		public override IEnumerable<string> ConfigErrors(HediffDef parentDef)
		{
			foreach (string item in base.ConfigErrors(parentDef))
			{
				yield return item;
			}
			if (tools != null)
			{
				Tool dupeTool = tools.SelectMany(delegate(Tool lhs)
				{
					HediffCompProperties_VerbGiver hediffCompProperties_VerbGiver = (HediffCompProperties_VerbGiver)this;
					return from rhs in tools
					where lhs != rhs && lhs.id == rhs.id
					select rhs;
				}).FirstOrDefault();
				if (dupeTool != null)
				{
					yield return $"duplicate hediff tool id {dupeTool.id}";
				}
				foreach (Tool t in tools)
				{
					foreach (string item2 in t.ConfigErrors())
					{
						yield return item2;
					}
				}
			}
		}
	}
}
