using System.Collections.Generic;

namespace Verse
{
	public class TreeNode_ThingCategory : TreeNode
	{
		public ThingCategoryDef catDef;

		public string Label => catDef.label;

		public string LabelCap => Label.CapitalizeFirst();

		public IEnumerable<TreeNode_ThingCategory> ChildCategoryNodesAndThis
		{
			get
			{
				foreach (ThingCategoryDef other in catDef.ThisAndChildCategoryDefs)
				{
					yield return other.treeNode;
				}
			}
		}

		public IEnumerable<TreeNode_ThingCategory> ChildCategoryNodes
		{
			get
			{
				foreach (ThingCategoryDef other in catDef.childCategories)
				{
					yield return other.treeNode;
				}
			}
		}

		public TreeNode_ThingCategory(ThingCategoryDef def)
		{
			catDef = def;
		}

		public override string ToString()
		{
			return catDef.defName;
		}
	}
}
