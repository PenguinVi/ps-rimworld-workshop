using RimWorld;
using System.Collections.Generic;

namespace Verse.AI
{
	public class JobDriver_AttackStatic : JobDriver
	{
		private bool startedIncapacitated;

		private int numAttacksMade;

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref startedIncapacitated, "startedIncapacitated", defaultValue: false);
			Scribe_Values.Look(ref numAttacksMade, "numAttacksMade", 0);
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			yield return Toils_Misc.ThrowColonistAttackingMote(TargetIndex.A);
			yield return new Toil
			{
				initAction = delegate
				{
					Pawn pawn2 = TargetThingA as Pawn;
					if (pawn2 != null)
					{
						startedIncapacitated = pawn2.Downed;
					}
					pawn.pather.StopDead();
				},
				tickAction = delegate
				{
					if (!TargetA.IsValid)
					{
						EndJobWith(JobCondition.Succeeded);
					}
					else
					{
						if (TargetA.HasThing)
						{
							Pawn pawn = TargetA.Thing as Pawn;
							if (TargetA.Thing.Destroyed || (pawn != null && !startedIncapacitated && pawn.Downed))
							{
								EndJobWith(JobCondition.Succeeded);
								return;
							}
						}
						if (numAttacksMade >= job.maxNumStaticAttacks && !((JobDriver)this).pawn.stances.FullBodyBusy)
						{
							EndJobWith(JobCondition.Succeeded);
						}
						else if (((JobDriver)this).pawn.TryStartAttack(TargetA))
						{
							numAttacksMade++;
						}
						else if (!((JobDriver)this).pawn.stances.FullBodyBusy)
						{
							Verb verb = ((JobDriver)this).pawn.TryGetAttackVerb(TargetA.Thing, !((JobDriver)this).pawn.IsColonist);
							if (job.endIfCantShootTargetFromCurPos && (verb == null || !verb.CanHitTargetFrom(((JobDriver)this).pawn.Position, TargetA)))
							{
								EndJobWith(JobCondition.Incompletable);
							}
							else if (job.endIfCantShootInMelee)
							{
								if (verb == null)
								{
									EndJobWith(JobCondition.Incompletable);
								}
								else
								{
									float num = verb.verbProps.EffectiveMinRange(TargetA, ((JobDriver)this).pawn);
									if ((float)((JobDriver)this).pawn.Position.DistanceToSquared(TargetA.Cell) < num * num && ((JobDriver)this).pawn.Position.AdjacentTo8WayOrInside(TargetA.Cell))
									{
										EndJobWith(JobCondition.Incompletable);
									}
								}
							}
						}
					}
				},
				defaultCompleteMode = ToilCompleteMode.Never
			};
		}
	}
}
