using RimWorld;
using System.Collections.Generic;

namespace Verse.AI
{
	public class JobDriver_HaulToContainer : JobDriver
	{
		protected const TargetIndex CarryThingIndex = TargetIndex.A;

		protected const TargetIndex DestIndex = TargetIndex.B;

		protected const TargetIndex PrimaryDestIndex = TargetIndex.C;

		public Thing ThingToCarry => (Thing)job.GetTarget(TargetIndex.A);

		public Thing Container => (Thing)job.GetTarget(TargetIndex.B);

		private int Duration => (Container != null && Container is Building) ? Container.def.building.haulToContainerDuration : 0;

		public override string GetReport()
		{
			Thing thing = null;
			thing = ((pawn.CurJob != job || pawn.carryTracker.CarriedThing == null) ? base.TargetThingA : pawn.carryTracker.CarriedThing);
			if (thing == null || !job.targetB.HasThing)
			{
				return "ReportHaulingUnknown".Translate();
			}
			return "ReportHaulingTo".Translate(thing.Label, job.targetB.Thing.LabelShort.Named("DESTINATION"), thing.Named("THING"));
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo target = base.job.GetTarget(TargetIndex.A);
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			if (!pawn.Reserve(target, job, 1, -1, null, errorOnFailed2))
			{
				return false;
			}
			pawn = base.pawn;
			target = base.job.GetTarget(TargetIndex.B);
			job = base.job;
			errorOnFailed2 = errorOnFailed;
			if (!pawn.Reserve(target, job, 1, -1, null, errorOnFailed2))
			{
				return false;
			}
			base.pawn.ReserveAsManyAsPossible(base.job.GetTargetQueue(TargetIndex.A), base.job);
			base.pawn.ReserveAsManyAsPossible(base.job.GetTargetQueue(TargetIndex.B), base.job);
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDestroyedOrNull(TargetIndex.A);
			this.FailOnDestroyedNullOrForbidden(TargetIndex.B);
			this.FailOn(() => TransporterUtility.WasLoadingCanceled(Container));
			this.FailOn(delegate
			{
				ThingOwner thingOwner = Container.TryGetInnerInteractableThingOwner();
				if (thingOwner != null && !thingOwner.CanAcceptAnyOf(ThingToCarry))
				{
					return true;
				}
				IHaulDestination haulDestination = Container as IHaulDestination;
				if (haulDestination != null && !haulDestination.Accepts(ThingToCarry))
				{
					return true;
				}
				return false;
			});
			Toil getToHaulTarget = Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.ClosestTouch).FailOnSomeonePhysicallyInteracting(TargetIndex.A);
			yield return getToHaulTarget;
			yield return Toils_Construct.UninstallIfMinifiable(TargetIndex.A).FailOnSomeonePhysicallyInteracting(TargetIndex.A);
			yield return Toils_Haul.StartCarryThing(TargetIndex.A, putRemainderInQueue: false, subtractNumTakenFromJobCount: true);
			yield return Toils_Haul.JumpIfAlsoCollectingNextTargetInQueue(getToHaulTarget, TargetIndex.A);
			Toil carryToContainer = Toils_Haul.CarryHauledThingToContainer();
			yield return carryToContainer;
			yield return Toils_Goto.MoveOffTargetBlueprint(TargetIndex.B);
			Toil prepare = Toils_General.Wait(Duration, TargetIndex.B);
			prepare.WithProgressBarToilDelay(TargetIndex.B);
			yield return prepare;
			yield return Toils_Construct.MakeSolidThingFromBlueprintIfNecessary(TargetIndex.B, TargetIndex.C);
			yield return Toils_Haul.DepositHauledThingInContainer(TargetIndex.B, TargetIndex.C);
			yield return Toils_Haul.JumpToCarryToNextContainerIfPossible(carryToContainer, TargetIndex.C);
		}
	}
}
