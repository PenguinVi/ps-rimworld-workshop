using RimWorld.Planet;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimWorld
{
	public class StorytellerComp_CategoryIndividualMTBByBiome : StorytellerComp
	{
		protected StorytellerCompProperties_CategoryIndividualMTBByBiome Props => (StorytellerCompProperties_CategoryIndividualMTBByBiome)props;

		public override IEnumerable<FiringIncident> MakeIntervalIncidents(IIncidentTarget target)
		{
			if (target is World)
			{
				yield break;
			}
			List<IncidentDef> allIncidents = DefDatabase<IncidentDef>.AllDefsListForReading;
			for (int i = 0; i < allIncidents.Count; i++)
			{
				IncidentDef inc = allIncidents[i];
				if (inc.category != Props.category)
				{
					continue;
				}
				BiomeDef biome = Find.WorldGrid[target.Tile].biome;
				if (inc.mtbDaysByBiome == null)
				{
					continue;
				}
				MTBByBiome entry = inc.mtbDaysByBiome.Find((MTBByBiome x) => x.biome == biome);
				if (entry == null)
				{
					continue;
				}
				float mtb = entry.mtbDays;
				if (Props.applyCaravanVisibility)
				{
					Caravan caravan = target as Caravan;
					if (caravan != null)
					{
						mtb /= caravan.Visibility;
					}
					else
					{
						Map map = target as Map;
						if (map != null && map.Parent.def.isTempIncidentMapOwner)
						{
							IEnumerable<Pawn> pawns = map.mapPawns.SpawnedPawnsInFaction(Faction.OfPlayer).Concat(map.mapPawns.PrisonersOfColonySpawned);
							mtb /= CaravanVisibilityCalculator.Visibility(pawns, caravanMovingNow: false);
						}
					}
				}
				if (Rand.MTBEventOccurs(mtb, 60000f, 1000f))
				{
					IncidentParms parms = GenerateParms(inc.category, target);
					if (inc.Worker.CanFireNow(parms))
					{
						yield return new FiringIncident(inc, this, parms);
					}
				}
			}
		}

		public override string ToString()
		{
			return base.ToString() + " " + Props.category;
		}
	}
}
