using System.Collections.Generic;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_TendPatient : JobDriver
	{
		private bool usesMedicine;

		private const int BaseTendDuration = 600;

		protected Thing MedicineUsed => job.targetB.Thing;

		protected Pawn Deliveree => (Pawn)job.targetA.Thing;

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref usesMedicine, "usesMedicine", defaultValue: false);
		}

		public override void Notify_Starting()
		{
			base.Notify_Starting();
			usesMedicine = (MedicineUsed != null);
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo target = Deliveree;
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			if (!pawn.Reserve(target, job, 1, -1, null, errorOnFailed2))
			{
				return false;
			}
			if (usesMedicine)
			{
				int num = base.pawn.Map.reservationManager.CanReserveStack(base.pawn, MedicineUsed, 10);
				if (num > 0)
				{
					pawn = base.pawn;
					target = MedicineUsed;
					job = base.job;
					int maxPawns = 10;
					int stackCount = Mathf.Min(num, Medicine.GetMedicineCountToFullyHeal(Deliveree));
					errorOnFailed2 = errorOnFailed;
					if (pawn.Reserve(target, job, maxPawns, stackCount, null, errorOnFailed2))
					{
						goto IL_00b7;
					}
				}
				return false;
			}
			goto IL_00b7;
			IL_00b7:
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDespawnedNullOrForbidden(TargetIndex.A);
			this.FailOn(delegate
			{
				if (!WorkGiver_Tend.GoodLayingStatusForTend(Deliveree, pawn))
				{
					return true;
				}
				if (MedicineUsed != null && pawn.Faction == Faction.OfPlayer)
				{
					if (Deliveree.playerSettings == null)
					{
						return true;
					}
					if (!Deliveree.playerSettings.medCare.AllowsMedicine(MedicineUsed.def))
					{
						return true;
					}
				}
				if (pawn == Deliveree && pawn.Faction == Faction.OfPlayer && !pawn.playerSettings.selfTend)
				{
					return true;
				}
				return false;
			});
			AddEndCondition(delegate
			{
				if (pawn.Faction == Faction.OfPlayer && HealthAIUtility.ShouldBeTendedNowByPlayer(Deliveree))
				{
					return JobCondition.Ongoing;
				}
				if (pawn.Faction != Faction.OfPlayer && Deliveree.health.HasHediffsNeedingTend())
				{
					return JobCondition.Ongoing;
				}
				return JobCondition.Succeeded;
			});
			this.FailOnAggroMentalState(TargetIndex.A);
			Toil reserveMedicine = null;
			if (usesMedicine)
			{
				reserveMedicine = Toils_Tend.ReserveMedicine(TargetIndex.B, Deliveree).FailOnDespawnedNullOrForbidden(TargetIndex.B);
				yield return reserveMedicine;
				yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.ClosestTouch).FailOnDespawnedNullOrForbidden(TargetIndex.B);
				yield return Toils_Tend.PickupMedicine(TargetIndex.B, Deliveree).FailOnDestroyedOrNull(TargetIndex.B);
				yield return Toils_Haul.CheckForGetOpportunityDuplicate(reserveMedicine, TargetIndex.B, TargetIndex.None, takeFromValidStorage: true);
			}
			PathEndMode interactionCell = (Deliveree == pawn) ? PathEndMode.OnCell : PathEndMode.InteractionCell;
			Toil gotoToil = Toils_Goto.GotoThing(TargetIndex.A, interactionCell);
			yield return gotoToil;
			int duration = (int)(1f / pawn.GetStatValue(StatDefOf.MedicalTendSpeed) * 600f);
			yield return Toils_General.Wait(duration).FailOnCannotTouch(TargetIndex.A, interactionCell).WithProgressBarToilDelay(TargetIndex.A)
				.PlaySustainerOrSound(SoundDefOf.Interact_Tend);
			yield return Toils_Tend.FinalizeTend(Deliveree);
			if (usesMedicine)
			{
				yield return new Toil
				{
					initAction = delegate
					{
						if (MedicineUsed.DestroyedOrNull())
						{
							Thing thing = HealthAIUtility.FindBestMedicine(pawn, Deliveree);
							if (thing != null)
							{
								job.targetB = thing;
								JumpToToil(reserveMedicine);
							}
						}
					}
				};
			}
			yield return Toils_Jump.Jump(gotoToil);
		}

		public override void Notify_DamageTaken(DamageInfo dinfo)
		{
			base.Notify_DamageTaken(dinfo);
			if (dinfo.Def.ExternalViolenceFor(pawn) && pawn.Faction != Faction.OfPlayer && pawn == Deliveree)
			{
				pawn.jobs.CheckForJobOverride();
			}
		}
	}
}
