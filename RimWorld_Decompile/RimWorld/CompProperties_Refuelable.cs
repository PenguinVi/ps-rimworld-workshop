using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace RimWorld
{
	public class CompProperties_Refuelable : CompProperties
	{
		public float fuelConsumptionRate = 1f;

		public float fuelCapacity = 2f;

		public float initialFuelPercent;

		public float autoRefuelPercent = 0.3f;

		public float fuelConsumptionPerTickInRain;

		public ThingFilter fuelFilter;

		public bool destroyOnNoFuel;

		public bool consumeFuelOnlyWhenUsed;

		public bool showFuelGizmo;

		public bool targetFuelLevelConfigurable;

		public float initialConfigurableTargetFuelLevel;

		public bool drawOutOfFuelOverlay = true;

		public float minimumFueledThreshold;

		public bool drawFuelGaugeInMap;

		public bool atomicFueling;

		private float fuelMultiplier = 1f;

		public bool factorByDifficulty;

		public string fuelLabel;

		public string fuelGizmoLabel;

		public string outOfFuelMessage;

		public string fuelIconPath;

		private Texture2D fuelIcon;

		public string FuelLabel => fuelLabel.NullOrEmpty() ? "Fuel".Translate() : fuelLabel;

		public string FuelGizmoLabel => fuelGizmoLabel.NullOrEmpty() ? "Fuel".Translate() : fuelGizmoLabel;

		public Texture2D FuelIcon
		{
			get
			{
				if (fuelIcon == null)
				{
					if (!fuelIconPath.NullOrEmpty())
					{
						fuelIcon = ContentFinder<Texture2D>.Get(fuelIconPath);
					}
					else
					{
						ThingDef thingDef = (fuelFilter.AnyAllowedDef == null) ? ThingDefOf.Chemfuel : fuelFilter.AnyAllowedDef;
						fuelIcon = thingDef.uiIcon;
					}
				}
				return fuelIcon;
			}
		}

		public float FuelMultiplierCurrentDifficulty
		{
			get
			{
				if (factorByDifficulty)
				{
					return fuelMultiplier / Find.Storyteller.difficulty.maintenanceCostFactor;
				}
				return fuelMultiplier;
			}
		}

		public CompProperties_Refuelable()
		{
			compClass = typeof(CompRefuelable);
		}

		public override void ResolveReferences(ThingDef parentDef)
		{
			base.ResolveReferences(parentDef);
			fuelFilter.ResolveReferences();
		}

		public override IEnumerable<string> ConfigErrors(ThingDef parentDef)
		{
			foreach (string item in base.ConfigErrors(parentDef))
			{
				yield return item;
			}
			if (destroyOnNoFuel && initialFuelPercent <= 0f)
			{
				yield return "Refuelable component has destroyOnNoFuel, but initialFuelPercent <= 0";
			}
			if ((!consumeFuelOnlyWhenUsed || fuelConsumptionPerTickInRain > 0f) && parentDef.tickerType != TickerType.Normal)
			{
				yield return $"Refuelable component set to consume fuel per tick, but parent tickertype is {parentDef.tickerType} instead of {TickerType.Normal}";
			}
		}

		public override IEnumerable<StatDrawEntry> SpecialDisplayStats(StatRequest req)
		{
			foreach (StatDrawEntry item in base.SpecialDisplayStats(req))
			{
				yield return item;
			}
			if (((ThingDef)req.Def).building.IsTurret)
			{
				StatCategoryDef building = StatCategoryDefOf.Building;
				string label = "RearmCost".Translate();
				string valueString = GenLabel.ThingLabel(fuelFilter.AnyAllowedDef, null, (int)(fuelCapacity / FuelMultiplierCurrentDifficulty)).CapitalizeFirst();
				string overrideReportText = "RearmCostExplanation".Translate();
				yield return new StatDrawEntry(building, label, valueString, 0, overrideReportText);
				building = StatCategoryDefOf.Building;
				overrideReportText = "ShotsBeforeRearm".Translate();
				valueString = ((int)fuelCapacity).ToString();
				label = "ShotsBeforeRearmExplanation".Translate();
				yield return new StatDrawEntry(building, overrideReportText, valueString, 0, label);
			}
		}
	}
}
