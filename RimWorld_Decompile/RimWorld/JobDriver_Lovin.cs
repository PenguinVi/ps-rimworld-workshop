using System.Collections.Generic;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_Lovin : JobDriver
	{
		private int ticksLeft;

		private TargetIndex PartnerInd = TargetIndex.A;

		private TargetIndex BedInd = TargetIndex.B;

		private const int TicksBetweenHeartMotes = 100;

		private static readonly SimpleCurve LovinIntervalHoursFromAgeCurve = new SimpleCurve
		{
			new CurvePoint(16f, 1.5f),
			new CurvePoint(22f, 1.5f),
			new CurvePoint(30f, 4f),
			new CurvePoint(50f, 12f),
			new CurvePoint(75f, 36f)
		};

		private Pawn Partner => (Pawn)(Thing)job.GetTarget(PartnerInd);

		private Building_Bed Bed => (Building_Bed)(Thing)job.GetTarget(BedInd);

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref ticksLeft, "ticksLeft", 0);
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo target = Partner;
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			int result;
			if (pawn.Reserve(target, job, 1, -1, null, errorOnFailed2))
			{
				pawn = base.pawn;
				target = Bed;
				job = base.job;
				int sleepingSlotsCount = Bed.SleepingSlotsCount;
				int stackCount = 0;
				errorOnFailed2 = errorOnFailed;
				result = (pawn.Reserve(target, job, sleepingSlotsCount, stackCount, null, errorOnFailed2) ? 1 : 0);
			}
			else
			{
				result = 0;
			}
			return (byte)result != 0;
		}

		public override bool CanBeginNowWhileLyingDown()
		{
			return JobInBedUtility.InBedOrRestSpotNow(pawn, job.GetTarget(BedInd));
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDespawnedOrNull(BedInd);
			this.FailOnDespawnedOrNull(PartnerInd);
			this.FailOn(() => !Partner.health.capacities.CanBeAwake);
			this.KeepLyingDown(BedInd);
			yield return Toils_Bed.ClaimBedIfNonMedical(BedInd);
			yield return Toils_Bed.GotoBed(BedInd);
			yield return new Toil
			{
				initAction = delegate
				{
					if (Partner.CurJob == null || Partner.CurJob.def != JobDefOf.Lovin)
					{
						Job newJob = new Job(JobDefOf.Lovin, pawn, Bed);
						Partner.jobs.StartJob(newJob, JobCondition.InterruptForced);
						ticksLeft = (int)(2500f * Mathf.Clamp(Rand.Range(0.1f, 1.1f), 0.1f, 2f));
					}
					else
					{
						ticksLeft = 9999999;
					}
				},
				defaultCompleteMode = ToilCompleteMode.Instant
			};
			Toil doLovin = Toils_LayDown.LayDown(BedInd, hasBed: true, lookForOtherJobs: false, canSleep: false, gainRestAndHealth: false);
			doLovin.FailOn(() => Partner.CurJob == null || Partner.CurJob.def != JobDefOf.Lovin);
			doLovin.AddPreTickAction(delegate
			{
				ticksLeft--;
				if (ticksLeft <= 0)
				{
					ReadyForNextToil();
				}
				else if (pawn.IsHashIntervalTick(100))
				{
					MoteMaker.ThrowMetaIcon(pawn.Position, pawn.Map, ThingDefOf.Mote_Heart);
				}
			});
			doLovin.AddFinishAction(delegate
			{
				Thought_Memory newThought = (Thought_Memory)ThoughtMaker.MakeThought(ThoughtDefOf.GotSomeLovin);
				pawn.needs.mood.thoughts.memories.TryGainMemory(newThought, Partner);
				pawn.mindState.canLovinTick = Find.TickManager.TicksGame + GenerateRandomMinTicksToNextLovin(pawn);
			});
			doLovin.socialMode = RandomSocialMode.Off;
			yield return doLovin;
		}

		private int GenerateRandomMinTicksToNextLovin(Pawn pawn)
		{
			if (DebugSettings.alwaysDoLovin)
			{
				return 100;
			}
			float centerX = LovinIntervalHoursFromAgeCurve.Evaluate(pawn.ageTracker.AgeBiologicalYearsFloat);
			centerX = Rand.Gaussian(centerX, 0.3f);
			if (centerX < 0.5f)
			{
				centerX = 0.5f;
			}
			return (int)(centerX * 2500f);
		}
	}
}
