using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_ExtinguishSelf : JobDriver
	{
		protected Fire TargetFire => (Fire)job.targetA.Thing;

		public override string GetReport()
		{
			if (TargetFire != null && TargetFire.parent != null)
			{
				return "ReportExtinguishingFireOn".Translate(TargetFire.parent.LabelCap, TargetFire.parent.Named("TARGET"));
			}
			return "ReportExtinguishingFire".Translate();
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			yield return new Toil
			{
				defaultCompleteMode = ToilCompleteMode.Delay,
				defaultDuration = 150
			};
			Toil killFire = new Toil
			{
				initAction = delegate
				{
					TargetFire.Destroy();
					pawn.records.Increment(RecordDefOf.FiresExtinguished);
				}
			};
			killFire.FailOnDestroyedOrNull(TargetIndex.A);
			killFire.defaultCompleteMode = ToilCompleteMode.Instant;
			yield return killFire;
		}
	}
}
