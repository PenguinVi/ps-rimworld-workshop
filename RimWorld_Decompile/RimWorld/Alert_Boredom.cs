using System;
using System.Collections.Generic;
using System.Text;
using Verse;

namespace RimWorld
{
	public class Alert_Boredom : Alert
	{
		private const float JoyNeedThreshold = 426f / (565f * (float)Math.PI);

		public Alert_Boredom()
		{
			defaultLabel = "Boredom".Translate();
			defaultPriority = AlertPriority.Medium;
		}

		public override AlertReport GetReport()
		{
			return AlertReport.CulpritsAre(BoredPawns());
		}

		public override string GetExplanation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			Pawn pawn = null;
			foreach (Pawn item in BoredPawns())
			{
				stringBuilder.AppendLine("   " + item.Label);
				if (pawn == null)
				{
					pawn = item;
				}
			}
			string value = JoyUtility.JoyKindsOnMapString(pawn.Map);
			return "BoredomDesc".Translate(stringBuilder.ToString().TrimEndNewlines(), pawn.LabelShort, value, pawn.Named("PAWN"));
		}

		private IEnumerable<Pawn> BoredPawns()
		{
			foreach (Pawn p in PawnsFinder.AllMaps_FreeColonistsSpawned)
			{
				if ((p.needs.joy.CurLevelPercentage < 426f / (565f * (float)Math.PI) || p.GetTimeAssignment() == TimeAssignmentDefOf.Joy) && p.needs.joy.tolerances.BoredOfAllAvailableJoyKinds(p))
				{
					yield return p;
				}
			}
		}
	}
}
