using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public abstract class WorkGiver_ConstructAffectFloor : WorkGiver_Scanner
	{
		protected abstract DesignationDef DesDef
		{
			get;
		}

		public override PathEndMode PathEndMode => PathEndMode.Touch;

		public override IEnumerable<IntVec3> PotentialWorkCellsGlobal(Pawn pawn)
		{
			if (pawn.Faction == Faction.OfPlayer)
			{
				foreach (Designation des in pawn.Map.designationManager.SpawnedDesignationsOfDef(DesDef))
				{
					yield return des.target.Cell;
				}
			}
		}

		public override bool HasJobOnCell(Pawn pawn, IntVec3 c, bool forced = false)
		{
			if (!c.IsForbidden(pawn) && pawn.Map.designationManager.DesignationAt(c, DesDef) != null)
			{
				LocalTargetInfo target = c;
				ReservationLayerDef floor = ReservationLayerDefOf.Floor;
				bool ignoreOtherReservations = forced;
				if (pawn.CanReserve(target, 1, -1, floor, ignoreOtherReservations))
				{
					return true;
				}
			}
			return false;
		}
	}
}
