using System.Collections.Generic;
using System.Text;
using Verse;

namespace RimWorld
{
	public abstract class Alert_Thought : Alert
	{
		protected string explanationKey;

		private static List<Thought> tmpThoughts = new List<Thought>();

		protected abstract ThoughtDef Thought
		{
			get;
		}

		private IEnumerable<Pawn> AffectedPawns()
		{
			foreach (Pawn p in PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists_NoCryptosleep)
			{
				if (p.Dead)
				{
					Log.Error("Dead pawn in PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists:" + p);
				}
				else
				{
					p.needs.mood.thoughts.GetAllMoodThoughts(tmpThoughts);
					try
					{
						ThoughtDef requiredDef = Thought;
						for (int i = 0; i < tmpThoughts.Count; i++)
						{
							if (tmpThoughts[i].def == requiredDef)
							{
								yield return p;
								break;
							}
						}
					}
					finally
					{
						base._003C_003E__Finally0();
					}
				}
			}
		}

		public override AlertReport GetReport()
		{
			return AlertReport.CulpritsAre(AffectedPawns());
		}

		public override string GetExplanation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (Pawn item in AffectedPawns())
			{
				stringBuilder.AppendLine("    " + item.LabelShort);
			}
			return explanationKey.Translate(stringBuilder.ToString());
		}
	}
}
