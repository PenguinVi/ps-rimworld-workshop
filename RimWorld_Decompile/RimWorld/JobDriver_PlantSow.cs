using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_PlantSow : JobDriver
	{
		private float sowWorkDone;

		private Plant Plant => (Plant)job.GetTarget(TargetIndex.A).Thing;

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref sowWorkDone, "sowWorkDone", 0f);
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo targetA = base.job.targetA;
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			return pawn.Reserve(targetA, job, 1, -1, null, errorOnFailed2);
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.Touch).FailOn(() => PlantUtility.AdjacentSowBlocker(job.plantDefToSow, TargetA.Cell, Map) != null).FailOn(() => !job.plantDefToSow.CanEverPlantAt(TargetLocA, Map));
			Toil sowToil = new Toil();
			sowToil.initAction = delegate
			{
				TargetThingA = GenSpawn.Spawn(job.plantDefToSow, TargetLocA, Map);
				pawn.Reserve(TargetThingA, sowToil.actor.CurJob);
				Plant plant3 = (Plant)TargetThingA;
				plant3.Growth = 0f;
				plant3.sown = true;
			};
			sowToil.tickAction = delegate
			{
				Pawn actor = sowToil.actor;
				if (actor.skills != null)
				{
					actor.skills.Learn(SkillDefOf.Plants, 0.085f);
				}
				float statValue = actor.GetStatValue(StatDefOf.PlantWorkSpeed);
				float num = statValue;
				Plant plant2 = Plant;
				if (plant2.LifeStage != 0)
				{
					Log.Error(this + " getting sowing work while not in Sowing life stage.");
				}
				sowWorkDone += num;
				if (sowWorkDone >= plant2.def.plant.sowWork)
				{
					plant2.Growth = 0.05f;
					Map.mapDrawer.MapMeshDirty(plant2.Position, MapMeshFlag.Things);
					actor.records.Increment(RecordDefOf.PlantsSown);
					ReadyForNextToil();
				}
			};
			sowToil.defaultCompleteMode = ToilCompleteMode.Never;
			sowToil.FailOnDespawnedNullOrForbidden(TargetIndex.A);
			sowToil.FailOnCannotTouch(TargetIndex.A, PathEndMode.Touch);
			sowToil.WithEffect(EffecterDefOf.Sow, TargetIndex.A);
			sowToil.WithProgressBar(TargetIndex.A, () => sowWorkDone / Plant.def.plant.sowWork, interpolateBetweenActorAndTarget: true);
			sowToil.PlaySustainerOrSound(() => SoundDefOf.Interact_Sow);
			sowToil.AddFinishAction(delegate
			{
				if (TargetThingA != null)
				{
					Plant plant = (Plant)sowToil.actor.CurJob.GetTarget(TargetIndex.A).Thing;
					if (sowWorkDone < plant.def.plant.sowWork && !TargetThingA.Destroyed)
					{
						TargetThingA.Destroy();
					}
				}
			});
			sowToil.activeSkill = (() => SkillDefOf.Plants);
			yield return sowToil;
		}
	}
}
