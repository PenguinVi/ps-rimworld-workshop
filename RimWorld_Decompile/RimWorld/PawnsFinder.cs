using RimWorld.Planet;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimWorld
{
	public static class PawnsFinder
	{
		public static IEnumerable<Pawn> AllMapsWorldAndTemporary_AliveOrDead
		{
			get
			{
				foreach (Pawn item in AllMapsWorldAndTemporary_Alive)
				{
					yield return item;
				}
				if (Find.World != null)
				{
					foreach (Pawn item2 in Find.WorldPawns.AllPawnsDead)
					{
						yield return item2;
					}
				}
				foreach (Pawn item3 in Temporary_Dead)
				{
					yield return item3;
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsWorldAndTemporary_Alive
		{
			get
			{
				foreach (Pawn allMap in AllMaps)
				{
					yield return allMap;
				}
				if (Find.World != null)
				{
					foreach (Pawn item in Find.WorldPawns.AllPawnsAlive)
					{
						yield return item;
					}
				}
				foreach (Pawn item2 in Temporary_Alive)
				{
					yield return item2;
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps
		{
			get
			{
				if (Current.ProgramState != 0)
				{
					List<Map> maps = Find.Maps;
					for (int i = 0; i < maps.Count; i++)
					{
						foreach (Pawn allPawn in maps[i].mapPawns.AllPawns)
						{
							yield return allPawn;
						}
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_Spawned
		{
			get
			{
				if (Current.ProgramState == ProgramState.Entry)
				{
					yield break;
				}
				List<Map> maps = Find.Maps;
				for (int j = 0; j < maps.Count; j++)
				{
					List<Pawn> spawned = maps[j].mapPawns.AllPawnsSpawned;
					for (int i = 0; i < spawned.Count; i++)
					{
						yield return spawned[i];
					}
				}
			}
		}

		public static IEnumerable<Pawn> All_AliveOrDead
		{
			get
			{
				foreach (Pawn item in AllMapsWorldAndTemporary_AliveOrDead)
				{
					yield return item;
				}
				foreach (Pawn item2 in AllCaravansAndTravelingTransportPods_AliveOrDead)
				{
					yield return item2;
				}
			}
		}

		public static IEnumerable<Pawn> Temporary
		{
			get
			{
				List<List<Pawn>> makingPawnsList = PawnGroupKindWorker.pawnsBeingGeneratedNow;
				for (int m = 0; m < makingPawnsList.Count; m++)
				{
					List<Pawn> makingPawns = makingPawnsList[m];
					for (int i = 0; i < makingPawns.Count; i++)
					{
						yield return makingPawns[i];
					}
				}
				List<List<Thing>> makingThingsList = ThingSetMaker.thingsBeingGeneratedNow;
				for (int l = 0; l < makingThingsList.Count; l++)
				{
					List<Thing> makingThings = makingThingsList[l];
					for (int j = 0; j < makingThings.Count; j++)
					{
						Pawn p = makingThings[j] as Pawn;
						if (p != null)
						{
							yield return p;
						}
					}
				}
				if (Current.ProgramState == ProgramState.Playing || Find.GameInitData == null)
				{
					yield break;
				}
				List<Pawn> startingAndOptionalPawns = Find.GameInitData.startingAndOptionalPawns;
				for (int k = 0; k < startingAndOptionalPawns.Count; k++)
				{
					if (startingAndOptionalPawns[k] != null)
					{
						yield return startingAndOptionalPawns[k];
					}
				}
			}
		}

		public static IEnumerable<Pawn> Temporary_Alive
		{
			get
			{
				foreach (Pawn p in Temporary)
				{
					if (!p.Dead)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> Temporary_Dead
		{
			get
			{
				foreach (Pawn p in Temporary)
				{
					if (p.Dead)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive
		{
			get
			{
				foreach (Pawn allMap in AllMaps)
				{
					yield return allMap;
				}
				foreach (Pawn item in AllCaravansAndTravelingTransportPods_Alive)
				{
					yield return item;
				}
			}
		}

		public static IEnumerable<Pawn> AllCaravansAndTravelingTransportPods_Alive
		{
			get
			{
				foreach (Pawn p in AllCaravansAndTravelingTransportPods_AliveOrDead)
				{
					if (!p.Dead)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllCaravansAndTravelingTransportPods_AliveOrDead
		{
			get
			{
				if (Find.World == null)
				{
					yield break;
				}
				List<Caravan> caravans = Find.WorldObjects.Caravans;
				for (int k = 0; k < caravans.Count; k++)
				{
					List<Pawn> pawns = caravans[k].PawnsListForReading;
					for (int i = 0; i < pawns.Count; i++)
					{
						yield return pawns[i];
					}
				}
				List<TravelingTransportPods> travelingTransportPods = Find.WorldObjects.TravelingTransportPods;
				for (int j = 0; j < travelingTransportPods.Count; j++)
				{
					foreach (Pawn pawn in travelingTransportPods[j].Pawns)
					{
						yield return pawn;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_Colonists
		{
			get
			{
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.IsColonist)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists
		{
			get
			{
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.IsFreeColonist)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists_NoCryptosleep
		{
			get
			{
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.IsFreeColonist && !p.Suspended)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_OfPlayerFaction
		{
			get
			{
				Faction playerFaction = Faction.OfPlayer;
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.Faction == playerFaction)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_OfPlayerFaction_NoCryptosleep
		{
			get
			{
				Faction playerFaction = Faction.OfPlayer;
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.Faction == playerFaction && !p.Suspended)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_PrisonersOfColony
		{
			get
			{
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive)
				{
					if (p.IsPrisonerOfColony)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonistsAndPrisoners => AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonists.Concat(AllMapsCaravansAndTravelingTransportPods_Alive_PrisonersOfColony);

		public static IEnumerable<Pawn> AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonistsAndPrisoners_NoCryptosleep
		{
			get
			{
				foreach (Pawn p in AllMapsCaravansAndTravelingTransportPods_Alive_FreeColonistsAndPrisoners)
				{
					if (!p.Suspended)
					{
						yield return p;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_PrisonersOfColonySpawned
		{
			get
			{
				if (Current.ProgramState == ProgramState.Entry)
				{
					yield break;
				}
				List<Map> maps = Find.Maps;
				for (int j = 0; j < maps.Count; j++)
				{
					List<Pawn> prisonersOfColonySpawned = maps[j].mapPawns.PrisonersOfColonySpawned;
					for (int i = 0; i < prisonersOfColonySpawned.Count; i++)
					{
						yield return prisonersOfColonySpawned[i];
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_PrisonersOfColony
		{
			get
			{
				if (Current.ProgramState != 0)
				{
					List<Map> maps = Find.Maps;
					for (int i = 0; i < maps.Count; i++)
					{
						foreach (Pawn item in maps[i].mapPawns.PrisonersOfColony)
						{
							yield return item;
						}
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_FreeColonists
		{
			get
			{
				if (Current.ProgramState != 0)
				{
					List<Map> maps = Find.Maps;
					for (int i = 0; i < maps.Count; i++)
					{
						foreach (Pawn freeColonist in maps[i].mapPawns.FreeColonists)
						{
							yield return freeColonist;
						}
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_FreeColonistsSpawned
		{
			get
			{
				if (Current.ProgramState != 0)
				{
					List<Map> maps = Find.Maps;
					for (int i = 0; i < maps.Count; i++)
					{
						foreach (Pawn item in maps[i].mapPawns.FreeColonistsSpawned)
						{
							yield return item;
						}
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_FreeColonistsAndPrisonersSpawned
		{
			get
			{
				if (Current.ProgramState != 0)
				{
					List<Map> maps = Find.Maps;
					for (int i = 0; i < maps.Count; i++)
					{
						foreach (Pawn item in maps[i].mapPawns.FreeColonistsAndPrisonersSpawned)
						{
							yield return item;
						}
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_FreeColonistsAndPrisoners
		{
			get
			{
				List<Map> maps = Find.Maps;
				for (int i = 0; i < maps.Count; i++)
				{
					foreach (Pawn freeColonistsAndPrisoner in maps[i].mapPawns.FreeColonistsAndPrisoners)
					{
						yield return freeColonistsAndPrisoner;
					}
				}
			}
		}

		public static IEnumerable<Pawn> AllMaps_SpawnedPawnsInFaction(Faction faction)
		{
			List<Map> maps = Find.Maps;
			for (int j = 0; j < maps.Count; j++)
			{
				List<Pawn> spawnedPawnsInFaction = maps[j].mapPawns.SpawnedPawnsInFaction(faction);
				for (int i = 0; i < spawnedPawnsInFaction.Count; i++)
				{
					yield return spawnedPawnsInFaction[i];
				}
			}
		}
	}
}
