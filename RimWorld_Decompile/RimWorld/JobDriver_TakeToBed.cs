using System.Collections.Generic;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace RimWorld
{
	public class JobDriver_TakeToBed : JobDriver
	{
		private const TargetIndex TakeeIndex = TargetIndex.A;

		private const TargetIndex BedIndex = TargetIndex.B;

		protected Pawn Takee => (Pawn)job.GetTarget(TargetIndex.A).Thing;

		protected Building_Bed DropBed => (Building_Bed)job.GetTarget(TargetIndex.B).Thing;

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo target = Takee;
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			int result;
			if (pawn.Reserve(target, job, 1, -1, null, errorOnFailed2))
			{
				pawn = base.pawn;
				target = DropBed;
				job = base.job;
				int sleepingSlotsCount = DropBed.SleepingSlotsCount;
				int stackCount = 0;
				errorOnFailed2 = errorOnFailed;
				result = (pawn.Reserve(target, job, sleepingSlotsCount, stackCount, null, errorOnFailed2) ? 1 : 0);
			}
			else
			{
				result = 0;
			}
			return (byte)result != 0;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDestroyedOrNull(TargetIndex.A);
			this.FailOnDestroyedOrNull(TargetIndex.B);
			this.FailOnAggroMentalStateAndHostile(TargetIndex.A);
			this.FailOn(delegate
			{
				if (job.def.makeTargetPrisoner)
				{
					if (!DropBed.ForPrisoners)
					{
						return true;
					}
				}
				else if (DropBed.ForPrisoners != Takee.IsPrisoner)
				{
					return true;
				}
				return false;
			});
			yield return Toils_Bed.ClaimBedIfNonMedical(TargetIndex.B, TargetIndex.A);
			AddFinishAction(delegate
			{
				if (job.def.makeTargetPrisoner && Takee.ownership.OwnedBed == DropBed && Takee.Position != RestUtility.GetBedSleepingSlotPosFor(Takee, DropBed))
				{
					Takee.ownership.UnclaimBed();
				}
			});
			yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.ClosestTouch).FailOnDespawnedNullOrForbidden(TargetIndex.A).FailOnDespawnedNullOrForbidden(TargetIndex.B)
				.FailOn(() => job.def == JobDefOf.Arrest && !Takee.CanBeArrestedBy(pawn))
				.FailOn(() => !pawn.CanReach(DropBed, PathEndMode.OnCell, Danger.Deadly))
				.FailOn(() => job.def == JobDefOf.Rescue && !Takee.Downed)
				.FailOnSomeonePhysicallyInteracting(TargetIndex.A);
			yield return new Toil
			{
				initAction = delegate
				{
					if (job.def.makeTargetPrisoner)
					{
						Pawn pawn = (Pawn)job.targetA.Thing;
						pawn.GetLord()?.Notify_PawnAttemptArrested(pawn);
						GenClamor.DoClamor(pawn, 10f, ClamorDefOf.Harm);
						if (job.def == JobDefOf.Arrest && !pawn.CheckAcceptArrest(this.pawn))
						{
							this.pawn.jobs.EndCurrentJob(JobCondition.Incompletable);
						}
					}
				}
			};
			Toil startCarrying = Toils_Haul.StartCarryThing(TargetIndex.A).FailOnNonMedicalBedNotOwned(TargetIndex.B, TargetIndex.A);
			startCarrying.AddPreInitAction(CheckMakeTakeeGuest);
			yield return startCarrying;
			yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.Touch);
			yield return new Toil
			{
				initAction = delegate
				{
					CheckMakeTakeePrisoner();
					if (Takee.playerSettings == null)
					{
						Takee.playerSettings = new Pawn_PlayerSettings(Takee);
					}
				}
			};
			yield return Toils_Reserve.Release(TargetIndex.B);
			yield return new Toil
			{
				initAction = delegate
				{
					IntVec3 position = DropBed.Position;
					pawn.carryTracker.TryDropCarriedThing(position, ThingPlaceMode.Direct, out Thing _);
					if (!DropBed.Destroyed && (DropBed.owners.Contains(Takee) || (DropBed.Medical && DropBed.AnyUnoccupiedSleepingSlot) || Takee.ownership == null))
					{
						Takee.jobs.Notify_TuckedIntoBed(DropBed);
						if (Takee.RaceProps.Humanlike && job.def != JobDefOf.Arrest && !Takee.IsPrisonerOfColony)
						{
							Takee.relations.Notify_RescuedBy(pawn);
						}
						Takee.mindState.Notify_TuckedIntoBed();
					}
					if (Takee.IsPrisonerOfColony)
					{
						LessonAutoActivator.TeachOpportunity(ConceptDefOf.PrisonerTab, Takee, OpportunityType.GoodToKnow);
					}
				},
				defaultCompleteMode = ToilCompleteMode.Instant
			};
		}

		private void CheckMakeTakeePrisoner()
		{
			if (job.def.makeTargetPrisoner)
			{
				if (Takee.guest.Released)
				{
					Takee.guest.Released = false;
					Takee.guest.interactionMode = PrisonerInteractionModeDefOf.NoInteraction;
				}
				if (!Takee.IsPrisonerOfColony)
				{
					Takee.guest.CapturedBy(Faction.OfPlayer, pawn);
				}
			}
		}

		private void CheckMakeTakeeGuest()
		{
			if (!job.def.makeTargetPrisoner && Takee.Faction != Faction.OfPlayer && Takee.HostFaction != Faction.OfPlayer && Takee.guest != null && !Takee.IsWildMan())
			{
				Takee.guest.SetGuestStatus(Faction.OfPlayer);
			}
		}
	}
}
