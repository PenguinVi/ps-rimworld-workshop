using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public static class SiegeBlueprintPlacer
	{
		private static IntVec3 center;

		private static Faction faction;

		private static List<IntVec3> placedSandbagLocs = new List<IntVec3>();

		private const int MaxArtyCount = 2;

		public const float ArtyCost = 60f;

		private const int MinSandbagDistSquared = 36;

		private static readonly IntRange NumSandbagRange = new IntRange(2, 4);

		private static readonly IntRange SandbagLengthRange = new IntRange(2, 7);

		public static IEnumerable<Blueprint_Build> PlaceBlueprints(IntVec3 placeCenter, Map map, Faction placeFaction, float points)
		{
			center = placeCenter;
			faction = placeFaction;
			foreach (Blueprint_Build item in PlaceSandbagBlueprints(map))
			{
				yield return item;
			}
			foreach (Blueprint_Build item2 in PlaceArtilleryBlueprints(points, map))
			{
				yield return item2;
			}
		}

		private static bool CanPlaceBlueprintAt(IntVec3 root, Rot4 rot, ThingDef buildingDef, Map map)
		{
			return GenConstruct.CanPlaceBlueprintAt(buildingDef, root, rot, map).Accepted;
		}

		private static IEnumerable<Blueprint_Build> PlaceSandbagBlueprints(Map map)
		{
			placedSandbagLocs.Clear();
			int numSandbags = NumSandbagRange.RandomInRange;
			for (int i = 0; i < numSandbags; i++)
			{
				IntVec3 bagRoot2 = FindSandbagRoot(map);
				if (!bagRoot2.IsValid)
				{
					break;
				}
				Rot4 growDirA = (bagRoot2.x <= center.x) ? Rot4.East : Rot4.West;
				Rot4 growDirB = (bagRoot2.z <= center.z) ? Rot4.North : Rot4.South;
				foreach (Blueprint_Build item in MakeSandbagLine(bagRoot2, map, growDirA, SandbagLengthRange.RandomInRange))
				{
					yield return item;
				}
				bagRoot2 += growDirB.FacingCell;
				foreach (Blueprint_Build item2 in MakeSandbagLine(bagRoot2, map, growDirB, SandbagLengthRange.RandomInRange))
				{
					yield return item2;
				}
			}
		}

		private static IEnumerable<Blueprint_Build> MakeSandbagLine(IntVec3 root, Map map, Rot4 growDir, int maxLength)
		{
			IntVec3 cur = root;
			for (int i = 0; i < maxLength; i++)
			{
				if (!CanPlaceBlueprintAt(cur, Rot4.North, ThingDefOf.Sandbags, map))
				{
					break;
				}
				yield return GenConstruct.PlaceBlueprintForBuild(ThingDefOf.Sandbags, cur, map, Rot4.North, faction, null);
				placedSandbagLocs.Add(cur);
				cur += growDir.FacingCell;
			}
		}

		private static IEnumerable<Blueprint_Build> PlaceArtilleryBlueprints(float points, Map map)
		{
			IEnumerable<ThingDef> artyDefs = from def in DefDatabase<ThingDef>.AllDefs
			where def.building != null && def.building.buildingTags.Contains("Artillery_BaseDestroyer")
			select def;
			int numArtillery2 = Mathf.RoundToInt(points / 60f);
			numArtillery2 = Mathf.Clamp(numArtillery2, 1, 2);
			for (int i = 0; i < numArtillery2; i++)
			{
				Rot4 rot = Rot4.Random;
				ThingDef artyDef = artyDefs.RandomElement();
				IntVec3 artySpot = FindArtySpot(artyDef, rot, map);
				if (!artySpot.IsValid)
				{
					break;
				}
				yield return GenConstruct.PlaceBlueprintForBuild(artyDef, artySpot, map, rot, faction, ThingDefOf.Steel);
				points -= 60f;
			}
		}

		private static IntVec3 FindSandbagRoot(Map map)
		{
			CellRect cellRect = CellRect.CenteredOn(center, 13);
			cellRect.ClipInsideMap(map);
			CellRect cellRect2 = CellRect.CenteredOn(center, 8);
			cellRect2.ClipInsideMap(map);
			int num = 0;
			goto IL_002d;
			IL_002d:
			IntVec3 randomCell;
			while (true)
			{
				num++;
				if (num > 200)
				{
					return IntVec3.Invalid;
				}
				randomCell = cellRect.RandomCell;
				if (cellRect2.Contains(randomCell) || !map.reachability.CanReach(randomCell, center, PathEndMode.OnCell, TraverseMode.NoPassClosedDoors, Danger.Deadly) || !CanPlaceBlueprintAt(randomCell, Rot4.North, ThingDefOf.Sandbags, map))
				{
					continue;
				}
				bool flag = false;
				for (int i = 0; i < placedSandbagLocs.Count; i++)
				{
					float num2 = (placedSandbagLocs[i] - randomCell).LengthHorizontalSquared;
					if (num2 < 36f)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					break;
				}
			}
			return randomCell;
			IL_00f7:
			goto IL_002d;
		}

		private static IntVec3 FindArtySpot(ThingDef artyDef, Rot4 rot, Map map)
		{
			CellRect cellRect = CellRect.CenteredOn(center, 8);
			cellRect.ClipInsideMap(map);
			int num = 0;
			goto IL_0017;
			IL_0017:
			IntVec3 randomCell;
			do
			{
				num++;
				if (num > 200)
				{
					return IntVec3.Invalid;
				}
				randomCell = cellRect.RandomCell;
			}
			while (!map.reachability.CanReach(randomCell, center, PathEndMode.OnCell, TraverseMode.NoPassClosedDoors, Danger.Deadly) || randomCell.Roofed(map) || !CanPlaceBlueprintAt(randomCell, rot, artyDef, map));
			return randomCell;
			IL_007d:
			goto IL_0017;
		}
	}
}
