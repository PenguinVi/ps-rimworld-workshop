using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_SmoothWall : JobDriver
	{
		private float workLeft = -1000f;

		protected int BaseWorkAmount => 6500;

		protected DesignationDef DesDef => DesignationDefOf.SmoothWall;

		protected StatDef SpeedStat => StatDefOf.SmoothingSpeed;

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo targetA = base.job.targetA;
			Job job = base.job;
			bool errorOnFailed2 = errorOnFailed;
			int result;
			if (pawn.Reserve(targetA, job, 1, -1, null, errorOnFailed2))
			{
				pawn = base.pawn;
				targetA = base.job.targetA.Cell;
				job = base.job;
				errorOnFailed2 = errorOnFailed;
				result = (pawn.Reserve(targetA, job, 1, -1, null, errorOnFailed2) ? 1 : 0);
			}
			else
			{
				result = 0;
			}
			return (byte)result != 0;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOn(delegate
			{
				if (!job.ignoreDesignations && Map.designationManager.DesignationAt(TargetLocA, DesDef) == null)
				{
					return true;
				}
				return false;
			});
			this.FailOnDespawnedNullOrForbidden(TargetIndex.A);
			yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.Touch);
			Toil doWork = new Toil();
			doWork.initAction = delegate
			{
				workLeft = BaseWorkAmount;
			};
			doWork.tickAction = delegate
			{
				float num = (SpeedStat == null) ? 1f : doWork.actor.GetStatValue(SpeedStat);
				workLeft -= num;
				if (doWork.actor.skills != null)
				{
					doWork.actor.skills.Learn(SkillDefOf.Construction, 0.1f);
				}
				if (workLeft <= 0f)
				{
					DoEffect();
					Map.designationManager.DesignationAt(TargetLocA, DesDef)?.Delete();
					ReadyForNextToil();
				}
			};
			doWork.FailOnCannotTouch(TargetIndex.A, PathEndMode.Touch);
			doWork.WithProgressBar(TargetIndex.A, () => 1f - workLeft / (float)BaseWorkAmount);
			doWork.defaultCompleteMode = ToilCompleteMode.Never;
			doWork.activeSkill = (() => SkillDefOf.Construction);
			yield return doWork;
		}

		protected void DoEffect()
		{
			SmoothableWallUtility.Notify_SmoothedByPawn(SmoothableWallUtility.SmoothWall(base.TargetA.Thing, pawn), pawn);
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref workLeft, "workLeft", 0f);
		}
	}
}
