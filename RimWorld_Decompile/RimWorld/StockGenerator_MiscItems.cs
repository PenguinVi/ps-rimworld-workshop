using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimWorld
{
	public abstract class StockGenerator_MiscItems : StockGenerator
	{
		public override IEnumerable<Thing> GenerateThings(int forTile)
		{
			int count = countRange.RandomInRange;
			for (int i = 0; i < count; i++)
			{
				if (!(from t in DefDatabase<ThingDef>.AllDefs
				where HandlesThingDef(t) && t.tradeability.TraderCanSell() && (int)t.techLevel <= (int)maxTechLevelGenerate
				select t).TryRandomElementByWeight(SelectionWeight, out ThingDef finalDef))
				{
					break;
				}
				yield return MakeThing(finalDef);
			}
		}

		protected virtual Thing MakeThing(ThingDef def)
		{
			return StockGeneratorUtility.TryMakeForStockSingle(def, 1);
		}

		public override bool HandlesThingDef(ThingDef thingDef)
		{
			return thingDef.tradeability != 0 && (int)thingDef.techLevel <= (int)maxTechLevelBuy;
		}

		protected virtual float SelectionWeight(ThingDef thingDef)
		{
			return 1f;
		}
	}
}
