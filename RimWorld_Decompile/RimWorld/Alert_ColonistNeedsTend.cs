using System.Collections.Generic;
using System.Text;
using Verse;

namespace RimWorld
{
	public class Alert_ColonistNeedsTend : Alert
	{
		private IEnumerable<Pawn> NeedingColonists
		{
			get
			{
				foreach (Pawn p in PawnsFinder.AllMaps_FreeColonistsSpawned)
				{
					if (p.health.HasHediffsNeedingTendByPlayer(forAlert: true))
					{
						Building_Bed curBed = p.CurrentBed();
						if ((curBed == null || !curBed.Medical) && !Alert_ColonistNeedsRescuing.NeedsRescue(p))
						{
							yield return p;
						}
					}
				}
			}
		}

		public Alert_ColonistNeedsTend()
		{
			defaultLabel = "ColonistNeedsTreatment".Translate();
			defaultPriority = AlertPriority.High;
		}

		public override string GetExplanation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (Pawn needingColonist in NeedingColonists)
			{
				stringBuilder.AppendLine("    " + needingColonist.LabelShort);
			}
			return "ColonistNeedsTreatmentDesc".Translate(stringBuilder.ToString());
		}

		public override AlertReport GetReport()
		{
			return AlertReport.CulpritsAre(NeedingColonists);
		}
	}
}
