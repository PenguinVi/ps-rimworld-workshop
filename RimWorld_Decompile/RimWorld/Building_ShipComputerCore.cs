using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace RimWorld
{
	public class Building_ShipComputerCore : Building
	{
		private bool CanLaunchNow => !ShipUtility.LaunchFailReasons(this).Any();

		public override IEnumerable<Gizmo> GetGizmos()
		{
			foreach (Gizmo gizmo in base.GetGizmos())
			{
				yield return gizmo;
			}
			foreach (Gizmo item in ShipUtility.ShipStartupGizmos(this))
			{
				yield return item;
			}
			Command_Action launch = new Command_Action
			{
				action = TryLaunch,
				defaultLabel = "CommandShipLaunch".Translate(),
				defaultDesc = "CommandShipLaunchDesc".Translate()
			};
			if (!CanLaunchNow)
			{
				launch.Disable(ShipUtility.LaunchFailReasons(this).First());
			}
			if (ShipCountdown.CountingDown)
			{
				launch.Disable();
			}
			launch.hotKey = KeyBindingDefOf.Misc1;
			launch.icon = ContentFinder<Texture2D>.Get("UI/Commands/LaunchShip");
			yield return launch;
		}

		private void TryLaunch()
		{
			if (CanLaunchNow)
			{
				ShipCountdown.InitiateCountdown(this);
			}
		}
	}
}
