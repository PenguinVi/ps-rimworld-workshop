using System;
using System.Collections.Generic;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.Sound;

namespace RimWorld
{
	public abstract class JobDriver_PlantWork : JobDriver
	{
		private float workDone;

		protected float xpPerTick;

		protected const TargetIndex PlantInd = TargetIndex.A;

		protected Plant Plant => (Plant)job.targetA.Thing;

		protected virtual DesignationDef RequiredDesignation => null;

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			LocalTargetInfo target = base.job.GetTarget(TargetIndex.A);
			if (target.IsValid)
			{
				Pawn pawn = base.pawn;
				LocalTargetInfo target2 = target;
				Job job = base.job;
				bool errorOnFailed2 = errorOnFailed;
				if (!pawn.Reserve(target2, job, 1, -1, null, errorOnFailed2))
				{
					return false;
				}
			}
			base.pawn.ReserveAsManyAsPossible(base.job.GetTargetQueue(TargetIndex.A), base.job);
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			Init();
			yield return Toils_JobTransforms.MoveCurrentTargetIntoQueue(TargetIndex.A);
			Toil initExtractTargetFromQueue = Toils_JobTransforms.ClearDespawnedNullOrForbiddenQueuedTargets(TargetIndex.A, (RequiredDesignation == null) ? null : ((Func<Thing, bool>)((Thing t) => Map.designationManager.DesignationOn(t, RequiredDesignation) != null)));
			yield return initExtractTargetFromQueue;
			yield return Toils_JobTransforms.SucceedOnNoTargetInQueue(TargetIndex.A);
			yield return Toils_JobTransforms.ExtractNextTargetFromQueue(TargetIndex.A);
			Toil gotoThing = Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.Touch).JumpIfDespawnedOrNullOrForbidden(TargetIndex.A, initExtractTargetFromQueue);
			if (RequiredDesignation != null)
			{
				gotoThing.FailOnThingMissingDesignation(TargetIndex.A, RequiredDesignation);
			}
			yield return gotoThing;
			Toil cut = new Toil();
			cut.tickAction = delegate
			{
				Pawn actor = cut.actor;
				if (actor.skills != null)
				{
					actor.skills.Learn(SkillDefOf.Plants, xpPerTick);
				}
				float statValue = actor.GetStatValue(StatDefOf.PlantWorkSpeed);
				float num = statValue;
				Plant plant = Plant;
				num *= Mathf.Lerp(3.3f, 1f, plant.Growth);
				workDone += num;
				if (workDone >= plant.def.plant.harvestWork)
				{
					if (plant.def.plant.harvestedThingDef != null)
					{
						if (actor.RaceProps.Humanlike && plant.def.plant.harvestFailable && Rand.Value > actor.GetStatValue(StatDefOf.PlantHarvestYield))
						{
							Vector3 loc = (pawn.DrawPos + plant.DrawPos) / 2f;
							MoteMaker.ThrowText(loc, Map, "TextMote_HarvestFailed".Translate(), 3.65f);
						}
						else
						{
							int num2 = plant.YieldNow();
							if (num2 > 0)
							{
								Thing thing = ThingMaker.MakeThing(plant.def.plant.harvestedThingDef);
								thing.stackCount = num2;
								if (actor.Faction != Faction.OfPlayer)
								{
									thing.SetForbidden(value: true);
								}
								GenPlace.TryPlaceThing(thing, actor.Position, Map, ThingPlaceMode.Near);
								actor.records.Increment(RecordDefOf.PlantsHarvested);
							}
						}
					}
					plant.def.plant.soundHarvestFinish.PlayOneShot(actor);
					plant.PlantCollected();
					workDone = 0f;
					ReadyForNextToil();
				}
			};
			cut.FailOnDespawnedNullOrForbidden(TargetIndex.A);
			if (RequiredDesignation != null)
			{
				cut.FailOnThingMissingDesignation(TargetIndex.A, RequiredDesignation);
			}
			cut.FailOnCannotTouch(TargetIndex.A, PathEndMode.Touch);
			cut.defaultCompleteMode = ToilCompleteMode.Never;
			cut.WithEffect(EffecterDefOf.Harvest, TargetIndex.A);
			cut.WithProgressBar(TargetIndex.A, () => workDone / Plant.def.plant.harvestWork, interpolateBetweenActorAndTarget: true);
			cut.PlaySustainerOrSound(() => Plant.def.plant.soundHarvesting);
			cut.activeSkill = (() => SkillDefOf.Plants);
			yield return cut;
			Toil plantWorkDoneToil = PlantWorkDoneToil();
			if (plantWorkDoneToil != null)
			{
				yield return plantWorkDoneToil;
			}
			yield return Toils_Jump.Jump(initExtractTargetFromQueue);
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref workDone, "workDone", 0f);
		}

		protected virtual void Init()
		{
		}

		protected virtual Toil PlantWorkDoneToil()
		{
			return null;
		}
	}
}
