using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public abstract class JobDriver_AffectRoof : JobDriver
	{
		private float workLeft;

		private const TargetIndex CellInd = TargetIndex.A;

		private const TargetIndex GotoTargetInd = TargetIndex.B;

		private const float BaseWorkAmount = 65f;

		protected IntVec3 Cell => job.GetTarget(TargetIndex.A).Cell;

		protected abstract PathEndMode PathEndMode
		{
			get;
		}

		protected abstract void DoEffect();

		protected abstract bool DoWorkFailOn();

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref workLeft, "workLeft", 0f);
		}

		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			Pawn pawn = base.pawn;
			LocalTargetInfo target = Cell;
			Job job = base.job;
			ReservationLayerDef ceiling = ReservationLayerDefOf.Ceiling;
			bool errorOnFailed2 = errorOnFailed;
			return pawn.Reserve(target, job, 1, -1, ceiling, errorOnFailed2);
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDespawnedOrNull(TargetIndex.B);
			yield return Toils_Goto.Goto(TargetIndex.B, PathEndMode);
			Toil doWork = new Toil();
			doWork.initAction = delegate
			{
				workLeft = 65f;
			};
			doWork.tickAction = delegate
			{
				float statValue = doWork.actor.GetStatValue(StatDefOf.ConstructionSpeed);
				workLeft -= statValue;
				if (workLeft <= 0f)
				{
					DoEffect();
					ReadyForNextToil();
				}
			};
			doWork.FailOnCannotTouch(TargetIndex.B, PathEndMode);
			doWork.PlaySoundAtStart(SoundDefOf.Roof_Start);
			doWork.PlaySoundAtEnd(SoundDefOf.Roof_Finish);
			doWork.WithEffect(EffecterDefOf.RoofWork, TargetIndex.A);
			doWork.FailOn(DoWorkFailOn);
			doWork.WithProgressBar(TargetIndex.A, () => 1f - workLeft / 65f);
			doWork.defaultCompleteMode = ToilCompleteMode.Never;
			yield return doWork;
		}
	}
}
