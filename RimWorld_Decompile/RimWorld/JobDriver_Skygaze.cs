using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimWorld
{
	public class JobDriver_Skygaze : JobDriver
	{
		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			return true;
		}

		protected override IEnumerable<Toil> MakeNewToils()
		{
			yield return Toils_Goto.GotoCell(TargetIndex.A, PathEndMode.OnCell);
			Toil gaze = new Toil
			{
				initAction = delegate
				{
					pawn.jobs.posture = PawnPosture.LayingOnGroundFaceUp;
				},
				tickAction = delegate
				{
					float num = this.pawn.Map.gameConditionManager.AggregateSkyGazeJoyGainFactor(this.pawn.Map);
					Pawn pawn = this.pawn;
					float extraJoyGainFactor = num;
					JoyUtility.JoyTickCheckEnd(pawn, JoyTickFullJoyAction.EndJob, extraJoyGainFactor);
				},
				defaultCompleteMode = ToilCompleteMode.Delay,
				defaultDuration = job.def.joyDuration
			};
			gaze.FailOn(() => pawn.Position.Roofed(pawn.Map));
			gaze.FailOn(() => !JoyUtility.EnjoyableOutsideNow(pawn));
			yield return gaze;
		}

		public override string GetReport()
		{
			if (base.Map.gameConditionManager.ConditionIsActive(GameConditionDefOf.Eclipse))
			{
				return "WatchingEclipse".Translate();
			}
			if (base.Map.gameConditionManager.ConditionIsActive(GameConditionDefOf.Aurora))
			{
				return "WatchingAurora".Translate();
			}
			float num = GenCelestial.CurCelestialSunGlow(base.Map);
			if (num < 0.1f)
			{
				return "Stargazing".Translate();
			}
			if (num < 0.65f)
			{
				if (GenLocalDate.DayPercent(pawn) < 0.5f)
				{
					return "WatchingSunrise".Translate();
				}
				return "WatchingSunset".Translate();
			}
			return "CloudWatching".Translate();
		}
	}
}
