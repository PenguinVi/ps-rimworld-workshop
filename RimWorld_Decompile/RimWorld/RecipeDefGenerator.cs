using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimWorld
{
	public static class RecipeDefGenerator
	{
		public static IEnumerable<RecipeDef> ImpliedRecipeDefs()
		{
			foreach (RecipeDef item in DefsFromRecipeMakers().Concat(DrugAdministerDefs()))
			{
				yield return item;
			}
		}

		private static IEnumerable<RecipeDef> DefsFromRecipeMakers()
		{
			foreach (ThingDef def in from d in DefDatabase<ThingDef>.AllDefs
			where d.recipeMaker != null
			select d)
			{
				RecipeMakerProperties rm = def.recipeMaker;
				RecipeDef r = new RecipeDef
				{
					defName = "Make_" + def.defName,
					label = "RecipeMake".Translate(def.label),
					jobString = "RecipeMakeJobString".Translate(def.label),
					modContentPack = def.modContentPack,
					workAmount = rm.workAmount,
					workSpeedStat = rm.workSpeedStat,
					efficiencyStat = rm.efficiencyStat
				};
				if (def.MadeFromStuff)
				{
					IngredientCount ingredientCount = new IngredientCount();
					ingredientCount.SetBaseCount(def.costStuffCount);
					ingredientCount.filter.SetAllowAllWhoCanMake(def);
					r.ingredients.Add(ingredientCount);
					r.fixedIngredientFilter.SetAllowAllWhoCanMake(def);
					r.productHasIngredientStuff = true;
				}
				if (def.costList != null)
				{
					foreach (ThingDefCountClass cost in def.costList)
					{
						IngredientCount ingredientCount2 = new IngredientCount();
						ingredientCount2.SetBaseCount(cost.count);
						ingredientCount2.filter.SetAllow(cost.thingDef, allow: true);
						r.ingredients.Add(ingredientCount2);
					}
				}
				r.defaultIngredientFilter = rm.defaultIngredientFilter;
				r.products.Add(new ThingDefCountClass(def, rm.productCount));
				r.targetCountAdjustment = rm.targetCountAdjustment;
				r.skillRequirements = rm.skillRequirements.ListFullCopyOrNull();
				r.workSkill = rm.workSkill;
				r.workSkillLearnFactor = rm.workSkillLearnPerTick;
				r.unfinishedThingDef = rm.unfinishedThingDef;
				r.recipeUsers = rm.recipeUsers.ListFullCopyOrNull();
				r.effectWorking = rm.effectWorking;
				r.soundWorking = rm.soundWorking;
				r.researchPrerequisite = rm.researchPrerequisite;
				r.factionPrerequisiteTags = rm.factionPrerequisiteTags;
				yield return r;
			}
		}

		private static IEnumerable<RecipeDef> DrugAdministerDefs()
		{
			foreach (ThingDef def in from d in DefDatabase<ThingDef>.AllDefs
			where d.IsDrug
			select d)
			{
				RecipeDef r = new RecipeDef
				{
					defName = "Administer_" + def.defName,
					label = "RecipeAdminister".Translate(def.label),
					jobString = "RecipeAdministerJobString".Translate(def.label),
					workerClass = typeof(Recipe_AdministerIngestible),
					targetsBodyPart = false,
					anesthetize = false,
					surgerySuccessChanceFactor = 99999f,
					modContentPack = def.modContentPack,
					workAmount = def.ingestible.baseIngestTicks
				};
				IngredientCount ic = new IngredientCount();
				ic.SetBaseCount(1f);
				ic.filter.SetAllow(def, allow: true);
				r.ingredients.Add(ic);
				r.fixedIngredientFilter.SetAllow(def, allow: true);
				r.recipeUsers = new List<ThingDef>();
				foreach (ThingDef item in DefDatabase<ThingDef>.AllDefs.Where((ThingDef d) => d.category == ThingCategory.Pawn && d.race.IsFlesh))
				{
					r.recipeUsers.Add(item);
				}
				yield return r;
			}
		}
	}
}
