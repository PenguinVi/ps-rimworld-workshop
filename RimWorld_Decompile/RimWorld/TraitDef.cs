using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimWorld
{
	public class TraitDef : Def
	{
		public List<TraitDegreeData> degreeDatas = new List<TraitDegreeData>();

		public List<TraitDef> conflictingTraits = new List<TraitDef>();

		public List<WorkTypeDef> requiredWorkTypes = new List<WorkTypeDef>();

		public WorkTags requiredWorkTags;

		public List<WorkTypeDef> disabledWorkTypes = new List<WorkTypeDef>();

		public WorkTags disabledWorkTags;

		private float commonality = 1f;

		private float commonalityFemale = -1f;

		public bool allowOnHostileSpawn = true;

		public static TraitDef Named(string defName)
		{
			return DefDatabase<TraitDef>.GetNamed(defName);
		}

		public TraitDegreeData DataAtDegree(int degree)
		{
			for (int i = 0; i < degreeDatas.Count; i++)
			{
				if (degreeDatas[i].degree == degree)
				{
					return degreeDatas[i];
				}
			}
			Log.Error(defName + " found no data at degree " + degree + ", returning first defined.");
			return degreeDatas[0];
		}

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string item in base.ConfigErrors())
			{
				yield return item;
			}
			if (commonality < 0.001f && commonalityFemale < 0.001f)
			{
				yield return "TraitDef " + defName + " has 0 commonality.";
			}
			if (!degreeDatas.Any())
			{
				yield return defName + " has no degree datas.";
			}
			for (int i = 0; i < degreeDatas.Count; i++)
			{
				TraitDegreeData dd3 = degreeDatas[i];
				if ((from dd2 in degreeDatas
				where dd2.degree == dd3.degree
				select dd2).Count() > 1)
				{
					yield return ">1 datas for degree " + dd3.degree;
				}
			}
		}

		public bool ConflictsWith(Trait other)
		{
			if (other.def.conflictingTraits != null)
			{
				for (int i = 0; i < other.def.conflictingTraits.Count; i++)
				{
					if (other.def.conflictingTraits[i] == this)
					{
						return true;
					}
				}
			}
			return false;
		}

		public float GetGenderSpecificCommonality(Gender gender)
		{
			if (gender == Gender.Female && commonalityFemale >= 0f)
			{
				return commonalityFemale;
			}
			return commonality;
		}
	}
}
