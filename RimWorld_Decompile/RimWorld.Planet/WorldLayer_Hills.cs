using System;
using System.Collections;
using UnityEngine;
using Verse;

namespace RimWorld.Planet
{
	public class WorldLayer_Hills : WorldLayer
	{
		private static readonly FloatRange BaseSizeRange = new FloatRange(0.9f, 1.1f);

		private static readonly IntVec2 TexturesInAtlas = new IntVec2(2, 2);

		private static readonly FloatRange BasePosOffsetRange_SmallHills = new FloatRange(0f, 0.37f);

		private static readonly FloatRange BasePosOffsetRange_LargeHills = new FloatRange(0f, 0.2f);

		private static readonly FloatRange BasePosOffsetRange_Mountains = new FloatRange(0f, 0.08f);

		private static readonly FloatRange BasePosOffsetRange_ImpassableMountains = new FloatRange(0f, 0.08f);

		public override IEnumerable Regenerate()
		{
			IEnumerator enumerator = base.Regenerate().GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					yield return enumerator.Current;
				}
			}
			finally
			{
				IDisposable disposable;
				IDisposable disposable2 = disposable = (enumerator as IDisposable);
				if (disposable != null)
				{
					disposable2.Dispose();
				}
			}
			Rand.PushState();
			Rand.Seed = Find.World.info.Seed;
			WorldGrid grid = Find.WorldGrid;
			int tilesCount = grid.TilesCount;
			for (int i = 0; i < tilesCount; i++)
			{
				Tile tile = grid[i];
				Material material;
				FloatRange floatRange;
				switch (tile.hilliness)
				{
				case Hilliness.SmallHills:
					material = WorldMaterials.SmallHills;
					floatRange = BasePosOffsetRange_SmallHills;
					break;
				case Hilliness.LargeHills:
					material = WorldMaterials.LargeHills;
					floatRange = BasePosOffsetRange_LargeHills;
					break;
				case Hilliness.Mountainous:
					material = WorldMaterials.Mountains;
					floatRange = BasePosOffsetRange_Mountains;
					break;
				case Hilliness.Impassable:
					material = WorldMaterials.ImpassableMountains;
					floatRange = BasePosOffsetRange_ImpassableMountains;
					break;
				default:
					continue;
				}
				LayerSubMesh subMesh = GetSubMesh(material);
				Vector3 tileCenter = grid.GetTileCenter(i);
				Vector3 posForTangents = tileCenter;
				float magnitude = tileCenter.magnitude;
				tileCenter = (tileCenter + Rand.UnitVector3 * floatRange.RandomInRange * grid.averageTileSize).normalized * magnitude;
				WorldRendererUtility.PrintQuadTangentialToPlanet(tileCenter, posForTangents, BaseSizeRange.RandomInRange * grid.averageTileSize, 0.005f, subMesh, counterClockwise: false, randomizeRotation: true, printUVs: false);
				IntVec2 texturesInAtlas = TexturesInAtlas;
				int indexX = Rand.Range(0, texturesInAtlas.x);
				IntVec2 texturesInAtlas2 = TexturesInAtlas;
				int indexY = Rand.Range(0, texturesInAtlas2.z);
				IntVec2 texturesInAtlas3 = TexturesInAtlas;
				int x = texturesInAtlas3.x;
				IntVec2 texturesInAtlas4 = TexturesInAtlas;
				WorldRendererUtility.PrintTextureAtlasUVs(indexX, indexY, x, texturesInAtlas4.z, subMesh);
			}
			Rand.PopState();
			FinalizeMesh(MeshParts.All);
		}
	}
}
