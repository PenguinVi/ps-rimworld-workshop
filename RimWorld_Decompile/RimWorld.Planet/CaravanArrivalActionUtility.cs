using System;
using System.Collections.Generic;
using Verse;

namespace RimWorld.Planet
{
	public static class CaravanArrivalActionUtility
	{
		public static IEnumerable<FloatMenuOption> GetFloatMenuOptions<T>(Func<FloatMenuAcceptanceReport> acceptanceReportGetter, Func<T> arrivalActionGetter, string label, Caravan caravan, int pathDestination, WorldObject revalidateWorldClickTarget) where T : CaravanArrivalAction
		{
			FloatMenuAcceptanceReport rep = acceptanceReportGetter();
			if (!rep.Accepted && rep.FailReason.NullOrEmpty() && rep.FailMessage.NullOrEmpty())
			{
				yield break;
			}
			if (!rep.FailReason.NullOrEmpty())
			{
				yield return new FloatMenuOption(label + " (" + rep.FailReason + ")", null);
				yield break;
			}
			Action action = delegate
			{
				FloatMenuAcceptanceReport floatMenuAcceptanceReport2 = acceptanceReportGetter();
				if (floatMenuAcceptanceReport2.Accepted)
				{
					caravan.pather.StartPath(pathDestination, arrivalActionGetter(), repathImmediately: true);
				}
				else if (!floatMenuAcceptanceReport2.FailMessage.NullOrEmpty())
				{
					Messages.Message(floatMenuAcceptanceReport2.FailMessage, new GlobalTargetInfo(pathDestination), MessageTypeDefOf.RejectInput, historical: false);
				}
			};
			yield return new FloatMenuOption(label, action, MenuOptionPriority.Default, null, null, 0f, null, revalidateWorldClickTarget);
			if (Prefs.DevMode)
			{
				string label2 = label + " (Dev: instantly)";
				action = delegate
				{
					FloatMenuAcceptanceReport floatMenuAcceptanceReport = acceptanceReportGetter();
					if (floatMenuAcceptanceReport.Accepted)
					{
						caravan.Tile = pathDestination;
						caravan.pather.StopDead();
						T val = arrivalActionGetter();
						val.Arrived(caravan);
					}
					else if (!floatMenuAcceptanceReport.FailMessage.NullOrEmpty())
					{
						Messages.Message(floatMenuAcceptanceReport.FailMessage, new GlobalTargetInfo(pathDestination), MessageTypeDefOf.RejectInput, historical: false);
					}
				};
				yield return new FloatMenuOption(label2, action, MenuOptionPriority.Default, null, null, 0f, null, revalidateWorldClickTarget);
			}
		}
	}
}
