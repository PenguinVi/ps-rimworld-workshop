using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;

namespace RimWorld.Planet
{
	public class Site : MapParent
	{
		public string customLabel;

		public SiteCore core;

		public List<SitePart> parts = new List<SitePart>();

		public bool sitePartsKnown;

		public bool factionMustRemainHostile;

		public float desiredThreatPoints;

		private bool startedCountdown;

		private bool anyEnemiesInitially;

		private Material cachedMat;

		private static List<string> tmpSitePartsLabels = new List<string>();

		public override string Label
		{
			get
			{
				if (!customLabel.NullOrEmpty())
				{
					return customLabel;
				}
				if (MainSiteDef == SiteCoreDefOf.PreciousLump && core.parms.preciousLumpResources != null)
				{
					return "PreciousLumpLabel".Translate(core.parms.preciousLumpResources.label);
				}
				return MainSiteDef.label;
			}
		}

		public override Texture2D ExpandingIcon => MainSiteDef.ExpandingIconTexture;

		public override Material Material
		{
			get
			{
				if (cachedMat == null)
				{
					cachedMat = MaterialPool.MatFrom(color: (!MainSiteDef.applyFactionColorToSiteTexture || base.Faction == null) ? Color.white : base.Faction.Color, texPath: MainSiteDef.siteTexture, shader: ShaderDatabase.WorldOverlayTransparentLit, renderQueue: WorldMaterials.WorldObjectRenderQueue);
				}
				return cachedMat;
			}
		}

		public override bool AppendFactionToInspectString => MainSiteDef.applyFactionColorToSiteTexture || MainSiteDef.showFactionInInspectString;

		private SiteCoreOrPartBase MainSiteCoreOrPart
		{
			get
			{
				if (core.def == SiteCoreDefOf.Nothing && parts.Any())
				{
					return parts[0];
				}
				return core;
			}
		}

		private SiteCoreOrPartDefBase MainSiteDef => MainSiteCoreOrPart.Def;

		public override IEnumerable<GenStepWithParams> ExtraGenStepDefs
		{
			get
			{
				foreach (GenStepWithParams extraGenStepDef in base.ExtraGenStepDefs)
				{
					yield return extraGenStepDef;
				}
				GenStepParams coreGenStepParms = new GenStepParams
				{
					siteCoreOrPart = core
				};
				List<GenStepDef> coreGenStepDefs = core.def.ExtraGenSteps;
				for (int k = 0; k < coreGenStepDefs.Count; k++)
				{
					yield return new GenStepWithParams(coreGenStepDefs[k], coreGenStepParms);
				}
				for (int j = 0; j < parts.Count; j++)
				{
					GenStepParams partGenStepParams = new GenStepParams
					{
						siteCoreOrPart = parts[j]
					};
					List<GenStepDef> partGenStepDefs = parts[j].def.ExtraGenSteps;
					for (int i = 0; i < partGenStepDefs.Count; i++)
					{
						yield return new GenStepWithParams(partGenStepDefs[i], partGenStepParams);
					}
				}
			}
		}

		public string ApproachOrderString => (!MainSiteDef.approachOrderString.NullOrEmpty()) ? string.Format(MainSiteDef.approachOrderString, Label) : "ApproachSite".Translate(Label);

		public string ApproachingReportString => (!MainSiteDef.approachingReportString.NullOrEmpty()) ? string.Format(MainSiteDef.approachingReportString, Label) : "ApproachingSite".Translate(Label);

		public float ActualThreatPoints
		{
			get
			{
				float num = core.parms.threatPoints;
				for (int i = 0; i < parts.Count; i++)
				{
					num += parts[i].parms.threatPoints;
				}
				return num;
			}
		}

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref customLabel, "customLabel");
			Scribe_Deep.Look(ref core, "core");
			Scribe_Collections.Look(ref parts, "parts", LookMode.Deep);
			Scribe_Values.Look(ref startedCountdown, "startedCountdown", defaultValue: false);
			Scribe_Values.Look(ref anyEnemiesInitially, "anyEnemiesInitially", defaultValue: false);
			Scribe_Values.Look(ref sitePartsKnown, "sitePartsKnown", defaultValue: false);
			Scribe_Values.Look(ref factionMustRemainHostile, "factionMustRemainHostile", defaultValue: false);
			Scribe_Values.Look(ref desiredThreatPoints, "desiredThreatPoints", 0f);
			if (Scribe.mode == LoadSaveMode.PostLoadInit)
			{
				BackCompatibility.SitePostLoadInit(this);
			}
		}

		public override void Tick()
		{
			base.Tick();
			core.def.Worker.SiteCoreWorkerTick(this);
			for (int i = 0; i < parts.Count; i++)
			{
				parts[i].def.Worker.SitePartWorkerTick(this);
			}
			if (base.HasMap)
			{
				CheckStartForceExitAndRemoveMapCountdown();
			}
		}

		public override void PostMapGenerate()
		{
			base.PostMapGenerate();
			Map map = base.Map;
			core.def.Worker.PostMapGenerate(map);
			for (int i = 0; i < parts.Count; i++)
			{
				parts[i].def.Worker.PostMapGenerate(map);
			}
			anyEnemiesInitially = GenHostility.AnyHostileActiveThreatToPlayer(base.Map);
		}

		public override bool ShouldRemoveMapNow(out bool alsoRemoveWorldObject)
		{
			alsoRemoveWorldObject = true;
			return !base.Map.mapPawns.AnyPawnBlockingMapRemoval;
		}

		public override IEnumerable<FloatMenuOption> GetFloatMenuOptions(Caravan caravan)
		{
			foreach (FloatMenuOption floatMenuOption in base.GetFloatMenuOptions(caravan))
			{
				yield return floatMenuOption;
			}
			foreach (FloatMenuOption floatMenuOption2 in core.def.Worker.GetFloatMenuOptions(caravan, this))
			{
				yield return floatMenuOption2;
			}
		}

		public override IEnumerable<FloatMenuOption> GetTransportPodsFloatMenuOptions(IEnumerable<IThingHolder> pods, CompLaunchable representative)
		{
			foreach (FloatMenuOption transportPodsFloatMenuOption in base.GetTransportPodsFloatMenuOptions(pods, representative))
			{
				yield return transportPodsFloatMenuOption;
			}
			foreach (FloatMenuOption transportPodsFloatMenuOption2 in core.def.Worker.GetTransportPodsFloatMenuOptions(pods, representative, this))
			{
				yield return transportPodsFloatMenuOption2;
			}
		}

		public override IEnumerable<Gizmo> GetGizmos()
		{
			foreach (Gizmo gizmo in base.GetGizmos())
			{
				yield return gizmo;
			}
			if (base.HasMap && Find.WorldSelector.SingleSelectedObject == this)
			{
				yield return SettleInExistingMapUtility.SettleCommand(base.Map, requiresNoEnemies: true);
			}
		}

		private void CheckStartForceExitAndRemoveMapCountdown()
		{
			if (!startedCountdown && !GenHostility.AnyHostileActiveThreatToPlayer(base.Map))
			{
				startedCountdown = true;
				int num = Mathf.RoundToInt(core.def.forceExitAndRemoveMapCountdownDurationDays * 60000f);
				string text = (!anyEnemiesInitially) ? "MessageSiteCountdownBecauseNoEnemiesInitially".Translate(TimedForcedExit.GetForceExitAndRemoveMapCountdownTimeLeftString(num)) : "MessageSiteCountdownBecauseNoMoreEnemies".Translate(TimedForcedExit.GetForceExitAndRemoveMapCountdownTimeLeftString(num));
				Messages.Message(text, this, MessageTypeDefOf.PositiveEvent);
				GetComponent<TimedForcedExit>().StartForceExitAndRemoveMapCountdown(num);
				TaleRecorder.RecordTale(TaleDefOf.CaravanAssaultSuccessful, base.Map.mapPawns.FreeColonists.RandomElement());
			}
		}

		public override bool AllMatchingObjectsOnScreenMatchesWith(WorldObject other)
		{
			Site site = other as Site;
			return site != null && site.MainSiteDef == MainSiteDef;
		}

		public override string GetInspectString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(base.GetInspectString());
			if (sitePartsKnown)
			{
				if (stringBuilder.Length != 0)
				{
					stringBuilder.AppendLine();
				}
				tmpSitePartsLabels.Clear();
				for (int i = 0; i < parts.Count; i++)
				{
					if (!parts[i].def.alwaysHidden)
					{
						tmpSitePartsLabels.Add(parts[i].def.Worker.GetPostProcessedThreatLabel(this, parts[i]));
					}
				}
				if (tmpSitePartsLabels.Count == 0)
				{
					stringBuilder.Append("KnownSiteThreatsNone".Translate());
				}
				else if (tmpSitePartsLabels.Count == 1)
				{
					stringBuilder.Append("KnownSiteThreat".Translate(tmpSitePartsLabels[0].CapitalizeFirst()));
				}
				else
				{
					stringBuilder.Append("KnownSiteThreats".Translate(tmpSitePartsLabels.ToCommaList(useAnd: true).CapitalizeFirst()));
				}
			}
			return stringBuilder.ToString();
		}

		public override string GetDescription()
		{
			string text = MainSiteDef.description;
			string description = base.GetDescription();
			if (!description.NullOrEmpty())
			{
				if (!text.NullOrEmpty())
				{
					text += "\n\n";
				}
				text += description;
			}
			return text;
		}
	}
}
